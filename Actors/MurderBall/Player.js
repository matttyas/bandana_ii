function Player (x, y, color, controls) {
	this.x = x;
	this.y = y;
	this.color = color;
	this.controls = controls;
  this.width = 20;
  this.height = 20;

  this.movement = { x: 0, y: 0};
  this.moveVector = { x: 0, y: 0};

  this.name = 'Player';
  this.spawnCircle = false;
  this.spawnForce = 11;
  this.charging = false;

  this.speed = 4;

  this.renderMethod = 'square';
}

Player.prototype = new Actor();

Player.prototype.update = function() {
	var totalMovement = Math.abs(this.moveVector.x) + Math.abs(this.moveVector.y);

	if (this.moveVector.x != 0 && this.moveVector.y != 0) {
		this.x += (this.speed * this.moveVector.x) / Math.sqrt(2);
		this.y += (this.speed * this.moveVector.y) / Math.sqrt(2);
	} else {
		if ( this.moveVector.x != 0 || this.moveVector.y != 0) {
			this.x += this.speed * this.moveVector.x;
			this.y += this.speed * this.moveVector.y;
		}
	}

};

Player.prototype.handleKeyboard = function(keysDown, previousKeysDown) {

	if (this.controls[0] in keysDown) {
		this.moveVector.y = -1;
	}

	if (this.controls[1] in keysDown) {
		this.moveVector.y = 1;
	}

	if (this.controls[2] in keysDown) {
		this.moveVector.x = -1;
	}

	if (this.controls[3] in keysDown) {
		this.moveVector.x = 1;
	}

	if (!(this.controls[0] in keysDown) && !(this.controls[1] in keysDown)) {
		this.moveVector.y = 0;
	}

	if (!(this.controls[2] in keysDown) && !(this.controls[3] in keysDown)) {
		this.moveVector.x = 0;
	}

	//Player pushes down the tackle button for the first time spawn
	if (this.controls[4] in keysDown && !(this.controls[4] in previousKeysDown)) {
		this.charging = true;
		this.spawnForce = 12;
	}

	if (this.controls[4] in keysDown) {
		this.spawnForce += 0.4;
	}

	//Player has let go of tackle button.
	if (!(this.controls[4] in keysDown) && this.controls[4] in previousKeysDown) {
		this.spawnCircle = true;
		this.charging = false;
	}
}

Player.prototype.setSpawnCircle = function(spawnCircle) {
	this.spawnCircle = spawnCircle;
}

Player.prototype.scalarProduct = function(a, b) {
	if (!b) b = {x: 0, y: 1}; //origin
	return (a.x * b.x) + (a.y * b.y);
}
function CircleForce (x, y, color, maxWidth, parent) {
	this.x = x;
	this.y = y;
	this.color = color;

	this.renderMethod = 'circle';
	this.maxWidth = maxWidth
	this.width = 12;
	this.style = 'stroke';
	this.lineWidth = 4;
	this.parent = parent;
}

CircleForce.prototype = new Actor();

CircleForce.prototype.update = function () {

	this.x = this.parent.x + (this.parent.width / 2);
	this.y = this.parent.y + (this.parent.height / 2);

	if (this.width < this.maxWidth) {
		this.width += 1.2;
	} else {
		this.deleteMe = true;
	}
}
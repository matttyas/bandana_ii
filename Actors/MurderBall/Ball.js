function Ball (x, y, screenWidth, screenHeight) {
  this.x = x;
  this.y = y;
  this.width = 7;
  this.height = 7;

  this.movement.x = 0;
  this.movement.y = 0 ;

  this.color = '#000000';
  this.name = 'Ball';

  this.screenWidth = screenWidth;
  this.screenHeight = screenHeight;

  this.renderMethod = 'circle';
}

Ball.prototype = new Actor();

Ball.prototype.update = function() {

	this.stayInBounds();

	this.x += this.movement.x;
	this.y += this.movement.y;

	if (this.movement.x > 0) {
		this.movement.x -= 0.2;
		if (this.movement.x < 0) this.movement.x = 0;
	} else if (this.movement.x < 0) {
		this.movement.x += 0.2;
		if (this.movement.x > 0) this.movement.x = 0;
	}

	if (this.movement.y > 0) {
		this.movement.y -= 0.2;
		if (this.movement.y < 0) this.movement.y = 0;
	} else if (this.movement.y < 0) {
		this.movement.y += 0.2;
		if (this.movement.y > 0) this.movement.y = 0;
	}
};

Ball.prototype.reset = function() {

};

Ball.prototype.stayInBounds = function() {
	if ( this.x + this.width > this.screenWidth ) {
		this.movement.x *= -1;
	}

	if ( this.x < 0 ) {
		this.movement.x *= -1;
	}

	if (this.y + this.height > this.screenHeight ) {
		this.movement.y *= -1;
	}

	if ( this.y < 0 ) {
		this.movement.y *= -1;
	}
};

//a = {x: 0, y: 0, r: 0}
//b = {x: 0, y: 0, r: 0}
Ball.prototype.circlesIntersect = function(a, b) {
  var d = (a.r - b.r) ^ 2;
  var e = (a.x - b.x) ^ 2 + ( a.y - b.y) ^ 2;
  var f = (a.r + b.r) ^ 2;

  console.log(a);
  console.log(b);
  //return ((d <= e) && ( e <= f));
  return d <= e <= f;
};
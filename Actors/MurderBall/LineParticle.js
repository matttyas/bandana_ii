function LineParticle (x, y, color, angle) {
	this.x = x;
	this.y = y;
	this.color = color;
	this.name = 'LineParticle';
	this.angle = angle;
	this.length = 0;

	this.points = [ {x: x, y: y} ];
}

LineParticle.prototype = new Actor();
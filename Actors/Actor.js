
function Actor (x, y) {
  this.x = 0; //The x coordinate of the actor in the current scene.
	this.y = 0; //The y coordinate of the actor in the current scene.
	this.width = 0; //The actors width, used for collision detection and rendering if no render method is used.
	this.height = 0; //The actors height, used for collision detection and rendering if no render method is used.

	this.color = '#000000'; //The color drawn if no other rendering method is used

	this.movement = { //The amount the actor should move at the end of the update process, post physics.
		x: 0,
		y: 0
	};

	this.name = '';
	this.renderMethod = undefined; //If set then used to pick how we render an object.
}

Actor.prototype.update = function() {
	this.x += this.movement.x;
	this.y += this.movement.y;
}

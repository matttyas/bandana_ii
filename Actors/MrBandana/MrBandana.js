/*
  SPECIAL THANKS:

  Joe. - He gave me a piece of sandwhich when I needed it most.
  James - Who demanded more parallax scrolling.

*/
function MrBandana () {
	this.movementVector = {x: 0, y: 0};
	this.movement = {x: 0, y: 0}; //Mainly used for jumping.

	this.renderMethod = 'self';
	this.width = 10;
	this.height = 16;
	this.borderColor = '#000000';
	this.color = '#00FF00';

	this.x = 0;
	this.y = 0;
	this.name = 'MrBandana';

	this.speed = 3;
	this.sprint = false;
	this.onGround = false;
	this.faceRight = true;
	this.onWallLeft = false;
	this.onWallRight = false;

	this.wallJumpTimer = 0;
	this.wallJumpDelay = 5;

	this.jumpHeight = 6;
	this.obstacles = [];
	this.devices = [];

	this.jumpWindUpTimer = 0;
	this.gravity = 0.2;

	this.currentObstacleStandingOn = null;
	this.wasStandingOn = null;

	this.keysDown = {};

	this.framesSinceCollision = 0;
	this.debug = true;
	this.dead = false;

	this.doubleJump = false;
	this.numberJumps = 0;

	this.ladder = null;
	this.onLadder = false;

	this.climbingSpeed = 2;

	this.shinies = 0;
	this.shiniesGathered = []; //Shinies gathered since last death
}

MrBandana.prototype = new Actor();

MrBandana.prototype.update = function () {

	if (this.sprint) {
		this.speed = 5;
		this.climbingSpeed = 4;
	} else {
		this.speed = 3;
		this.climbingSpeed = 2;
	}

	if (this.onLadder) return;

	this.x += this.movementVector.x * this.speed;

	this.y += this.movement.y / 2;

	this.checkCollisions();

	this.y += this.movement.y / 2;
	//Apply Gravity

	if (!this.onGround) {
		this.movement.y += this.gravity;
		//if (this.wallJumpTimer == this.wallJumpDelay) console.log('Wall jump');
	}

	if (this.wallJumpTimer > 0) this.wallJumpTimer--;

	this.checkCollisions();

	if (this.currentObstacleStandingOn) {

		this.y += 1;
		var collisions = Physics.advancedCollision(this, this.currentObstacleStandingOn);
		this.y -= 1;

		if (this.currentObstacleStandingOn.moving) {
			this.y = this.currentObstacleStandingOn.y - this.height;
			this.onGround = true;
		}
		//Probs woalked off the edge.
		if (!collisions ) {
			this.onGround = false;
			this.jumps = 1;
			this.currentObstacleStandingOn = null;
		}

		if ( this.currentObstacleStandingOn && this.currentObstacleStandingOn.rocket && this.currentObstacleStandingOn.fire) {
			this.onGround = false;
			this.movement.y = this.currentObstacleStandingOn.speed * -1.5;
			this.currentObstacleStandingOn = null;
		}
	}

	this.y = Math.round(this.y);
	//Hit the floor
	/*if (!this.onGround && this.y + this.height > 316) {
		this.y = 300;
		this.onGround = true;
		this.movement.y = 0;
	}*/
};

MrBandana.prototype.handleKeyboard = function(keysDown, previousKeysDown) {
	this.keysDown = keysDown;

	if (32 in keysDown && !(32 in previousKeysDown)) { //Jump
  	//console.log('Jump');
  	this.jump(keysDown, previousKeysDown);
  }

	if (this.wallJumpTimer == 0) this.movementVector.x = 0;

	if (87 in keysDown) { //W = Climb Ladders
		if (this.ladder) {

			this.climbLadder();
		}
  }

  if (83 in keysDown) { //S = Duck? Drop through platforms
    if (this.ladder) {
    	this.climbDownLadder();
    } else {
    	this.checkLadders();
    }
  }

  if (65 in keysDown) { //A
  	if ( !this.onWallLeft && this.wallJumpTimer == 0) {
			this.movementVector.x = -1;
			this.faceRight = false;
		}
  }

  if (68 in keysDown) { //D
  	if ( !this.onWallRight && this.wallJumpTimer == 0) {
			this.movementVector.x = 1;
			this.faceRight = true;
		};
  }

  if (16 in keysDown) { //Shift = Sprint
  	this.sprint = true;
  } else {
  	this.sprint = false;
  }
};

//Jump! Jump! Jump! Jump!
MrBandana.prototype.jump = function(keysDown, previousKeysDown) {

	//Shouldn't jump if not on the ground. Maybe change to later allow double jump

	//Start jumping
	/*if (32 in keysDown && !(32 in previousKeysDown)){
		this.jumpWindUpTimer = 0;
	} else {
		this.jumpWindUpTimer++;
	}

	if (this.jumpWindUpTimer < 3 ) {
		this.movement.y += (this.jumpHeight * -1) / 3
	}*/

	// if (this.onWallRight) {
	// 	console.log('On Right Wall');
	// }

	if (this.onGround) {
		if (this.currentObstacleStandingOn.moving) {
			this.movement.y = this.currentObstacleStandingOn.speed * -1;
		}
		this.currentObstacleStandingOn = null;
		this.movement.y += this.jumpHeight * -1;
		this.onGround = false;
		this.jumps = 1;
		return;
	} else {
		if ( this.jumps < 2 && this.doubleJump ) {
			this.jumps = 2;
			this.movement.y = this.jumpHeight * -1;
		}
	}

	this.x+=1;
	this.checkCollisions();
	this.x -=1;

	if (this.onWallRight) {
		this.movement.y = this.jumpHeight * -1;
		this.movementVector.x = -1;
		this.wallJumpTimer = this.wallJumpDelay;
		this.jumps = 1;
	}

	this.x -= 1;
	this.checkCollisions();
	this.x += 1;

	if (this.onWallLeft) {
		this.movement.y = this.jumpHeight * -1;
		this.movementVector.x = 1;
		this.wallJumpTimer = this.wallJumpDelay;
		this.jumps = 1;
	}
};

MrBandana.prototype.climbLadder = function() {
	this.onGround = false;
	this.currentObstacleStandingOn = null;
	this.wasStandingOn = null;

	this.onLadder = true;
	this.y -= this.climbingSpeed;
	this.x = this.ladder.x + 2;


};

MrBandana.prototype.climbDownLadder = function() {
	if (!this.onGround) {
		this.onGround = false;
		this.currentObstacleStandingOn = null;
		this.wasStandingOn = null;

		this.onLadder = true;
		this.y += this.climbingSpeed;
		this.x = this.ladder.x + 2;

		if (this.y + this.height > this.ladder.y + this.ladder.height) {
			this.y = this.ladder.y + this.ladder.height - this.height;
			this.onLadder = false;
			this.ladder = null;
		}
	}
};

MrBandana.prototype.render = function(ctx, renderer) {
	ctx.fillStyle = this.borderColor;

	this.renderX = this.x - renderer.camera.x;
	this.renderY = this.y - renderer.camera.y;

	ctx.fillRect(this.renderX, this.renderY, this.width, this.height);

  ctx.fillStyle = this.color;
	ctx.fillRect(this.renderX + 1, this.renderY + 1, this.width - 2, this.height - 2);

	//Bandana!
	ctx.fillStyle = '#FF0000';
  ctx.fillRect( this.renderX + 1, this.renderY + 3, this.width - 2, 3);

  if (this.onLadder) return;

  //eye
  ctx.fillStyle = '#0000DD';
  if (this.faceRight) {
  	ctx.fillRect( this.renderX + 7, this.renderY + 4, 1, 1);
  } else {
  	ctx.fillRect( this.renderX + 2, this.renderY + 4, 1, 1);
  }

  if (this.debug) {
  	ctx.fillStyle = 'white';
  	ctx.font = '10px Arial';
  	ctx.fillText('x: ' + this.x, 10,10);
  	ctx.fillText('y: ' + this.y, 10,30);
  }
};

MrBandana.prototype.setObstacles = function(obstacles) {
  this.obstacles = obstacles;
};

MrBandana.prototype.setDevices = function(devices) {
	this.devices = devices;
};

MrBandana.prototype.gather = function(actor) {
	if (actor.name == 'Shiny') {
		this.shinies += 1;
		this.shiniesGathered.push(actor);
	}
}

MrBandana.prototype.checkCollisions = function() {

	//If on moving platform then we stick to it's y.
	if (this.inair) {
    var a = 'a';
	};

	if (this.currentObstacleStandingOn) {
		if ( this.y < this.currentObstacleStandingOn.y) this.y = this.currentObstacleStandingOn.y - this.height;
		this.y += 1;

		var collision = Physics.checkCollisionRects(this, this.currentObstacleStandingOn);
		if (!collision) {
			this.currentObstacleStandingOn = null;
		} else {
			this.y -= 1;
		}
	}

	var anyCollision = false;
	var setOnGround = false;

	for (var i = 0; i < this.obstacles.length; i++) {
		var collision = Physics.checkCollisionRects(this, this.obstacles[i]);

		var obstacle = this.obstacles[i];

		if (collision.collision) {
			var xOverlap = Math.max(0, Math.min(this.x + this.width, obstacle.x + obstacle.width) - Math.max(this.x, obstacle.x));
			var yOverlap = Math.max(0, Math.min(this.y + this.height, obstacle.y + obstacle.height) - Math.max(this.y, obstacle.y));

			var collisions = Physics.advancedCollision(this, obstacle);

			if (!collisions) {
				//Missed the edges. Apply overlap and bail
				this.y -= yOverlap;
				this.movement.y = 0;
				continue;
			}

			xOverlap = collisions[0].xOverlap;
			yOverlap = collisions[0].yOverlap;

			anyCollision = true;

			//Hit one side. Easy
			if (collisions.length >= 1) {
				if (yOverlap < xOverlap) {

					if (this.movement.y > 0) {
						this.y -= yOverlap;
						this.movement.y = 0;
						this.onGround = true;
						this.jumps = 0;
						this.currentObstacleStandingOn = obstacle;

					} else {
						this.onGround = false;
						this.y += yOverlap;
						this.movement.y = 0;
					}
				} else {

					if (this.movementVector.x > 0) {

						this.x -= xOverlap;
						this.movementVector.x = 0;
						this.onWallRight = true;
//						console.log('ON WALL RIGHT');
					} else {

						this.x += xOverlap;
						this.movementVector.x = 0;
						this.onWallLeft = true;
					}
				}
			} /*else {
				//Hit two or more sides. DONT PANIC
				if (this.y + this.height - this.gravity - 0.6< obstacle.y) {
					this.onGround = true;
					this.y -= yOverlap;
				}

				if (xOverlap < yOverlap) {
					if (this.movementVector.x > 0) {
						this.x += xOverlap;
						this.movementVector.x = 0;
						this.onWallRight = true;
					} else if (this.movementVector.x < 0) {
						this.x -= xOverlap;
						this.movementVector.x = 0;
						this.onWallLeft = true;
					} else {
						//I think this is only hit when we hit the bottom of a platform
						this.movement.y = 0;
						this.y += yOverlap;
					}
				}

			}*/

		}
	}

	if (this.currentObstacleStandingOn) {

	}

	if (!anyCollision ) {
		this.framesSinceCollision += 1;
		if ( this.framesSinceCollision > 1) {
			this.onWallRight = false;
			this.onWallLeft = false;
		}

		/*if (65 in this.keysDown) {
			this.onWallLeft = false;
		}

		if (68 in this.keysDown) {
			this.onWallRight = false;
		}*/

	} else {
		this.framesSinceCollision = 0;
	}
};

MrBandana.prototype.clippingLadder = function(ladder) {
  this.ladder = ladder;
};

//Check for ladders below the players feet. If present and relevent button pressed move down a little.
MrBandana.prototype.checkLadders = function() {
	this.y += 2;

	for (var i = 0; i < this.devices.length; i++) {
		if (this.devices[i].name != 'Ladder') continue;


		var collision = Physics.checkCollisionRects(this, this.devices[i]);
		if (collision.collision) {
			this.clippingLadder(this.devices[i]);
			this.onGround = false;
			this.climbDownLadder();
		}
	}
};
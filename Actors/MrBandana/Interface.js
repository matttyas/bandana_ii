function Interface() {
  this.renderMethod = 'self';
  this.shinies = 0;
}

Interface.prototype = new Actor();

Interface.prototype.update = function() {

};

Interface.prototype.render = function(ctx) {

	ctx.fillStyle = 'black';
	ctx.fillRect(750, 11, 9, 9);

	ctx.fillStyle = 'yellow';
	ctx.fillRect(751, 12, 7, 7);

  ctx.fillStyle = 'white';
  ctx.font = '10pt Arial';
  ctx.fillText(this.shinies, 770,20);
  //ctx.fillText('y: ' + this.y, 0,30);
};
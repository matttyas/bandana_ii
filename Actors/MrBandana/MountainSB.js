function MountainSB (x, y) {
	this.y = y || 300;
  this.renderMethod = 'self';
  this.width = 60;
  this.height = 70;
  this.offset = 0;
  this.x = x || 0;
  this.color = '#FFFFFF';
  this.transparency = 1;
  this.scrollSpeed = 6;
  this.heightOffset = 160;
  this.interval = 0;
}

MountainSB.prototype = new ScrollingBackground(this.x, this.y);

MountainSB.prototype.drawShape = function(ctx, startx, starty) {
  ctx.fillStyle = this.color;
  ctx.beginPath();

  ctx.moveTo(startx, starty);
  ctx.lineTo(startx + this.width / 2, starty - this.height);
  ctx.lineTo(startx + this.width, starty);
  ctx.lineTo(startx, starty);
  ctx.fill();
  ctx.closePath();
};

MountainSB.prototype.getYOffset = function(renderer) {
  return renderer.height + 30;
};

MountainSB.prototype.setDimensions = function(renderer) {
  this.height = renderer.height / 2 + this.heightOffset;
  this.width = (renderer.height / 2 ) + 30;
  this.interval = this.width;
  this.y = renderer.height + 30;
};
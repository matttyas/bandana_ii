function BackPanelTrigger (x, y, width, height, renderer) {
  this.renderMethod = 'none';
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.colors = [];
  this.renderer = renderer;
  this.useGradient = false;
  this.hideScrollingBack = false;
  this.scrollingBack = null;
  this.useBackParticleSystem = false;
}

BackPanelTrigger.prototype = new Actor();

BackPanelTrigger.prototype.handleCollision = function (collider) {
  if (this.useGradient) {
  	this.renderer.setUseGradientForBack(this.colors);
  	this.renderer.setGradientColors(this.colors);
  } else {
  	this.renderer.useBackColor(this.colors[0]);
  }

  this.renderer.setHideScrollingBack(this.hideScrollingBack);

  if (!this.hideScrollingBack) {
    if (this.scrollingBack) {
      this.renderer.useScrollingBack(this.scrollingBack);
    }
  }

  this.renderer.setUseBackParticleSystem(this.useBackParticleSystem);
};

BackPanelTrigger.prototype.setColors = function(colors) {
	this.colors = colors;
};

BackPanelTrigger.prototype.setUseGradient = function(useGradient) {
	this.useGradient = useGradient;
};

BackPanelTrigger.prototype.setHideScrollingBack = function(hideScrollingBack) {
  this.hideScrollingBack = hideScrollingBack;
};

BackPanelTrigger.prototype.setScrollingBack = function(scrollingBack) {
  this.scrollingBack = scrollingBack;
};

BackPanelTrigger.prototype.setUseBackParticleSystem = function (useBackParticleSystem) {
  this.useBackParticleSystem = useBackParticleSystem;
};
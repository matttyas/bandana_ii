function Switch (x, y, target) {
  this.name = 'Switch';
  this.x = x;
  this.y = y;
  this.on = false;
  this.width = 20;
  this.height = 20;
  this.renderMethod = 'self';
  this.targets = [target];
}

Switch.prototype = new Actor();

Switch.prototype.render = function(ctx, renderer) {

	var renderX = this.x - renderer.camera.x;
	var renderY = this.y - renderer.camera.y;

	//Border
	ctx.fillStyle = '#000000';
	ctx.fillRect(renderX, renderY, this.width, this.height);

	//Inside
	ctx.fillStyle = '#AAAAAA';
	ctx.fillRect(renderX + 1, renderY + 1, this.width - 2, this.height - 2);

	//Inner Border
	ctx.fillStyle = '#000000';
	ctx.fillRect(renderX + 5, renderY + 5, this.width - 10, this.height - 10)

	if (this.on) {
		ctx.fillStyle = '#00FF00';
	} else {
		ctx.fillStyle = '#FF0000';
	}

	ctx.fillRect(renderX + 6, renderY + 6, this.width - 12, this.height - 12);
};

Switch.prototype.handleCollision = function (hero) {
	if (!this.on) {
		this.on = true;

		for (var i = 0; i < this.targets.length; i++) {
			var switches = this.targets[i].getSwitches();
			for (var j = 0; j < switches.length; j++) {
				if (!switches[j].on) return;
			}

			this.targets[i].allSwitchesOn();
		}
	}
};

Switch.prototype.addTarget = function(target) {
	this.targets.push(target);
};
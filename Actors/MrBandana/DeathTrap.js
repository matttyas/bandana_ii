function DeathTrap (x, y, width, height, type) {
  this.type = type || 'spikes';
  this.renderMethod = 'self';
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.name = 'DeathTrap';
  //Will have to round width to be equal to the size of the repeating unit
}

DeathTrap.prototype = new Actor();

DeathTrap.prototype.render = function(ctx, renderer) {
	var renderX = this.x - renderer.camera.x;
  var renderY = this.y - renderer.camera.y;

  if (this.type = 'spikes') {
  	i = 0;
  	while ( i < this.width) {
  		ctx.fillStyle = '#000000';

  		ctx.beginPath();
      var startx = renderX + i;
      var starty = renderY + this.height;
      ctx.moveTo(startx, starty);
      ctx.lineTo(startx + 6, starty - this.height);
      ctx.lineTo(startx + 12, starty);
      ctx.lineTo(startx, starty);
      ctx.fill();
      ctx.closePath();

      ctx.fillStyle = '#FFFFFF';
      ctx.beginPath();
      var startx = renderX + i + 1;
      var starty = renderY + this.height;
      ctx.moveTo(startx, starty);
      ctx.lineTo(startx + 5, starty - this.height + 2);
      ctx.lineTo(startx + 10, starty);
      ctx.lineTo(startx, starty);
      ctx.fill();
      ctx.closePath();
      i += 10;
   }
  }
};

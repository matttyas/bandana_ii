function StalagtitesSB (x, y) {
	this.y = y || 0;
  this.renderMethod = 'self';
  this.width = 60;
  this.height = 100;
  this.offset = 0;
  this.x = x || 0;
  this.color = '#333333';
  this.transparency = 1;
  this.scrollSpeed = 6;
  this.heightOffset = 160;
  this.interval = this.width;
}

StalagtitesSB.prototype = new ScrollingBackground();

StalagtitesSB.prototype.drawShape = function(ctx, startx, starty, renderer) {
  ctx.fillStyle = this.color;
  ctx.beginPath();
  this.y = this.getYOffset(renderer);
  ctx.moveTo(startx, starty);
  ctx.lineTo(startx + this.width / 2, starty + this.height);
  ctx.lineTo(startx + this.width, starty);
  ctx.lineTo(startx, starty);
  ctx.fill();
  ctx.closePath();
};

StalagtitesSB.prototype.setDimensions = function(renderer) {

};
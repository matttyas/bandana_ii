function Door (x,y, height, colors) {

	this.backgroundColor
	if (colors) {
  	this.color = colors[0];
  	this.secondaryColor = colors[1] || '#000000';
  } else {
  	this.color = '#999999';
  	this.secondaryColor = '#000000';
  }

	this.x = x;
	this.y = y;
	this.height = height;
	this.width = 8;
	this.renderMethod = 'self';
	this.name = 'Door';

	this.closedHeight = height;
	this.open = false;

	this.closeDoor();
}

Door.prototype = new Actor();

Door.prototype.render = function(ctx, renderer) {
  var renderX = this.x - renderer.camera.x;
  var renderY = this.y - renderer.camera.y;

  //top box
  ctx.fillStyle = '#000000';
  ctx.fillRect(renderX, renderY, 10, 10);

  ctx.fillStyle = this.color;
  ctx.fillRect(renderX + 1, renderY + 1, 8, 8);

  if (this.open) {
  	ctx.fillStyle = '#00FF00';
  } else {
  	ctx.fillStyle = '#FF0000';
  }

  ctx.fillRect(renderX + 3, renderY + 3, 4, 4);

  if (this.open) return;

  ctx.fillStyle = '#000000';
  ctx.fillRect(renderX + 1, renderY + 10, this.width, this.height - 10);

  ctx.fillStyle = this.color;
  ctx.fillRect(renderX + 2, renderY + 10, this.width - 2, this.height - 11);
  //Background
};

Door.prototype.openDoor = function() {
	this.open = true;
	this.height = 10;
};

Door.prototype.closeDoor = function() {
	this.open = false;
	this.height = this.closedHeight;
};

Door.prototype.setSwitches = function(switches) {
	this.switches = switches;
};

Door.prototype.getSwitches = function() {
	return this.switches;
};

Door.prototype.allSwitchesOn = function() {
	this.openDoor();
};

Door.prototype.handleCollision = function (hero) {
  console.log('a');
};
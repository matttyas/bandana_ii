function FrontStalagtitesSB (x, y) {
	this.y = y || 0;
  this.renderMethod = 'self';

  this.interval = 0;
}

FrontStalagtitesSB.prototype = new ScrollingBackground();

FrontStalagtitesSB.prototype.drawShape = function(ctx, startx, starty, renderer) {
  ctx.fillStyle = this.color;
  ctx.beginPath();
  this.y = this.getYOffset(renderer);
  ctx.moveTo(startx, starty);
  ctx.lineTo(startx + this.width / 2, starty + this.height);
  ctx.lineTo(startx + this.width, starty);
  ctx.lineTo(startx, starty);
  ctx.fill();
  ctx.closePath();
};

FrontStalagtitesSB.prototype.setDimensions = function(renderer) {
  this.interval = 200;
};
function ScrollingBackground (x, y) {
	this.y = y || 300;
  this.renderMethod = 'self';
  this.width = 60;
  this.height = 70;
  this.offset = 0;
  this.verticalOffset = 0;
  this.x = x || 0;
  this.color = '#FFFFFF';
  this.transparency = 1;
  this.scrollSpeed = 6;
  this.heightOffset = 160;
  this.interval = 0;
  this.repeat = true;
}

ScrollingBackground.prototype = new Actor();

ScrollingBackground.prototype.render = function(ctx, renderer) {
  ctx.globalAlpha = this.transparency;

  this.setDimensions(renderer);
  var screenWidth = renderer.width;
  var reps = Math.ceil(screenWidth / this.width) + 1;
  for (var i = -1; i < reps; i++) {

    var startx = this.offset + this.x + (this.interval * i);
    var starty = this.y;
    this.drawShape(ctx, startx, starty, renderer);
  }
  ctx.globalAlpha = 1;
};

ScrollingBackground.prototype.drawShape = function (ctx, startx, starty, renderer) {
  //Overwrite with child classes.
};

ScrollingBackground.prototype.scroll = function(speed, moveVectorX) {

	if (! moveVectorX) return;

	this.offset += (speed / this.scrollSpeed) * (moveVectorX * -1);

  if ((this.offset > this.interval || this.offset < (this.interval * -1)) && this.repeat) {
  	this.offset = 0;
  }
};

ScrollingBackground.prototype.setTransparency = function(transparency) {
  this.transparency = transparency;
};

ScrollingBackground.prototype.setOffset = function(offset) {
	this.offset = offset;
};

ScrollingBackground.prototype.setColor = function(color) {
	this.color = color;
};


ScrollingBackground.prototype.setScrollSpeed = function(speed) {
  this.scrollSpeed = speed;
};

ScrollingBackground.prototype.setOffsetHalfWidth = function() {
  this.offset = this.width / 2;
};

ScrollingBackground.prototype.increaseHeight = function(height) {
	this.heightOffset += height;
};

ScrollingBackground.prototype.setInterval = function(interval) {
  this.interval = interval;
};

ScrollingBackground.prototype.setRepeat = function(repeat) {
  this.repeat = this.repeat;
};

ScrollingBackground.prototype.setY = function(y) {
  this.y = y;
};

//Overwrite
ScrollingBackground.prototype.getYOffset = function(renderer) {
  return 0;
};

ScrollingBackground.prototype.setDimensions = function(renderer) {
  //this.height = renderer.height / 2 + this.heightOffset;
  //this.width = (renderer.height / 2 ) + 30;
};
function Shiny(x, y, checkpointId) {
	this.renderMethod = 'self';
	this.gathered = false;
	this.name = 'Shiny';
	this.x = x;
	this.y = y;
	this.width = 9;
	this.height = 9;
	this.checkpointId = checkpointId;
	this.checkpoint = null; //Checkpoint shiny assigned to.
}

Shiny.prototype = new Actor();

Shiny.prototype.render = function(ctx, renderer, blur) {

	if (!this.gathered) {

		if (blur) renderer.blur(this);

		this.renderX = this.x - renderer.camera.x;
		this.renderY = this.y - renderer.camera.y;

		ctx.fillStyle = 'black';
		ctx.fillRect(this.renderX, this.renderY, this.width, this.height);

		ctx.fillStyle = 'yellow';
		ctx.fillRect(this.renderX + 1, this.renderY + 1, this.width - 2, this.height - 2);
	}

};

Shiny.prototype.handleCollision = function(collider) {
	if (!this.gathered) {
		collider.gather(this);
		this.gathered = true;
		this.deleteMe = true;
		this.checkpoint.checkAllGathered();
	}
};
function Ladder (x, y, height, colors) {
  this.renderMethod = 'self';
  this.name = 'Ladder';
  this.width = 14;
  this.x = x;
  this.y = y;
  this.height = height;
  if (colors) {
  	this.color = colors[0];
  	if (colors[1]) this.backColor = colors[1] || '#000000';
  } else {
  	this.color = '#FFFFFF';
  }
}

Ladder.prototype = new Actor();

Ladder.prototype.render = function(ctx, renderer) {
  var renderX = this.x - renderer.camera.x;
  var renderY = this.y - renderer.camera.y;

  //Back of ladder.
  if (this.backColor) {
    ctx.fillStyle = this.backColor;
    ctx.fillRect(renderX, renderY, this.width, this.height);
  }
    ctx.fillStyle = this.color;

  var rungs = Math.round(this.height / 10);


  //Sides
  ctx.fillRect(renderX, renderY, 2, this.height);
  ctx.fillRect(renderX + this.width - 2, renderY, 2, this.height);
  //Top to bottom
  ctx.fillRect(renderX, renderY, this.width, 3);
  for (var i = 1; i < rungs; i++) {
    ctx.fillRect(renderX, renderY + (i * 10), this.width, 3);
   // ctx.fillRect(renderX, renderY + (i * 10) + 7, this.width, 3);
  }
}

Ladder.prototype.handleCollision = function(hero) {
  hero.clippingLadder(this);
}
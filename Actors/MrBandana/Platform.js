function Platform (x, y, width, height, colors) {
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;

	this.color = '#AAAAAA';
	this.name = 'Platform';

	this.canMove = false;
	this.moving = false;
	this.switches = null;
	this.delay = 180; //If platform can move delay is the amount of time before it starts moving
	this.goingDown = false;
	this.rocket = false; //Rocket platforms shoot player up at end of journey.
	this.fire = false;

	if (colors) {
		this.color = colors[0];
	}

  this.renderMethod = 'self';
};

Platform.prototype = new Actor();

Platform.prototype.render = function(ctx, renderer) {

	this.renderX = this.x - renderer.camera.x;
	this.renderY = this.y - renderer.camera.y;

	if (this.canMove) {
		ctx.fillStyle = '#000000';
  	ctx.fillRect ( this.renderX, this.renderY, this.width, this.height);

		ctx.fillStyle = this.color;
  	ctx.fillRect ( this.renderX + 1, this.renderY + 1, this.width - 2, this.height - 2);

  	if (this.moving) {
  		ctx.strokeStyle = '#00FF00';
  		ctx.lineWidth = 1;
  		ctx.strokeRect ( this.renderX + 1, this.renderY + 1, this.width - 2, this.height - 2);
  	}
	} else {
		ctx.fillStyle = this.color;
  	ctx.fillRect ( this.renderX, this.renderY, this.width, this.height);
	}

};

Platform.prototype.update = function() {

	if (!this.canMove || !this.moving) return;
	if (this.countdown()) return;

	this.move();
};

//Returns true if still counting down
Platform.prototype.countdown = function() {
  if ( this.delay > 0) {
  	this.delay--
  	return true;
  } else {
  	return false;
  }
};

Platform.prototype.move = function() {

	var diffX = this.targetLocation.x - this.startLocation.x;
	var diffY = this.targetLocation.y - this.startLocation.y;

	var xDist = this.targetLocation.x - this.x;
	var yDist = this.targetLocation.y - this.y;

	this.fire = false;

	if (diffX < 0) {
		this.x -= this.speed;
	} else if (diffX > 0) {
		this.x += this.speed;
	}

	if (diffY < 0) {
		this.y -= this.speed;
		this.goingDown = false;
	} else if (diffY > 0 ) {
		this.y += this.speed;
		this.goingDown = true;
	}

	var newXDist = this.targetLocation.x - this.x;
	var newYDist = this.targetLocation.y - this.y;

	if ((newXDist <= 0 && xDist >= 0 || newXDist >= 0 && xDist <= 0)) {
		if ( this.targetLocation.x - this.startLocation.x != 0) {
			var oldTarget = {x: this.targetLocation.x , y: this.targetLocation.y };
			this.targetLocation = {y: this.startLocation.x, y: this.startLocation.y}
			this.startLocation = oldTarget;
		}
	}

	if (( newYDist <= 0 && yDist >= 0 || newYDist >= 0 && yDist <= 0)) {
		if (this.targetLocation.y - this.startLocation.y != 0) {

			if ( this.rocket && this.targetLocation.y < this.startLocation.y) {
				this.fire = true;
			}

			var oldTarget = {x: this.targetLocation.x , y: this.targetLocation.y };
			this.targetLocation = {x: this.startLocation.x, y: this.startLocation.y}
			this.startLocation = oldTarget;
		}
	}

	if (this.x == this.targetLocation.x && this.y == this.targetLocation.y) {

	}
}

Platform.prototype.setCanMove = function(canMove) {
	this.canMove = canMove;
};

Platform.prototype.setTargetLocation = function(x, y) {
	this.targetLocation = {x: 0, y: 0};
	this.startLocation = {x: 0, y: 0};

	this.targetLocation.x = x;
	this.targetLocation.y = y;

	this.startLocation.x = this.x;
	this.startLocation.y = this.y;
};

Platform.prototype.setSpeed = function(speed) {
	this.speed = speed || 2;
};

Platform.prototype.setSwitches = function(switches) {
	this.switches = switches;
};

Platform.prototype.getSwitches = function() {
	return this.switches;
};

Platform.prototype.allSwitchesOn = function() {
	this.moving = true;
};

Platform.prototype.setRocket = function(rocket) {
	this.rocket = rocket;
};
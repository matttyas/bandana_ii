//Class containing all scrolling elements for a level.

function LevelScrollingBackground () {
  this.renderMethod = 'self';
  this.elements = [];
}

LevelScrollingBackground.prototype = new Actor();

LevelScrollingBackground.prototype.render = function(ctx, renderer) {
  for (var i = 0; i < this.elements.length; i++) {
  	this.elements[i].render(ctx, renderer);
  }
};

LevelScrollingBackground.prototype.addElement = function(element) {
	this.elements.push(element);
};

LevelScrollingBackground.prototype.scroll = function(hero) {
	for (var i = 0; i < this.elements.length; i++) {
	  this.elements[i].scroll(hero.speed, hero.movementVector.x);
	}
};

//Email Discount Code: WH368 - Parking
function BackPanel (x, y, width, height, colors) {
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height
	this.renderMethod = 'self';
	this.name = 'BackPanel';

	this.color = '#AAAAAA';
	if (colors) {
		this.color = colors[0];
	}
	this.gradient = false;
}

BackPanel.prototype = new Actor();

BackPanel.prototype.render = function(ctx, renderer) {

	if (this.gradient) {
		var grd = ctx.createLinearGradient(this.x - renderer.camera.x, this.y - renderer.camera.y, this.x - renderer.camera.x, (this.y - renderer.camera.y) + this.height);
  	grd.addColorStop(0, this.gradient[0]);
  	grd.addColorStop(1, this.gradient[1]);
  	ctx.fillStyle = grd;
	} else {
		ctx.fillStyle = this.color;
	}

  ctx.fillRect ( this.x - renderer.camera.x, this.y - renderer.camera.y, this.width, this.height);
};

BackPanel.prototype.setGradient = function(color1, color2) {
	this.gradient = [color1, color2];
};
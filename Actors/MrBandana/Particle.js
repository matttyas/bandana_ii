function Particle(x, y, distance) {
	this.startX = x;
	this.startY = y;
  this.x = x;
  this.y = y;
  this.finalY = y - (Math.random() * 1000) + 200;
  this.deleteMe = false;
  //The further away a particle is the smaller it is.
  distance = distance || 0;

  this.distance = 1 - distance;
  this.width = 5 * this.distance;
  this.height = 5 * this.distance;

  //PARALLAX SCROLLING?

  this.color = 'rgb(255, 69, 0)';
  this.transparency = Math.random() + 0.6;
  if (this.transparency > 1) this.transparency = 1;
}

Particle.prototype = new Actor();

Particle.prototype.generate = function(x, y) {
	return new Particle(x, y);
};

Particle.prototype.update = function() {
	this.y -= 0.5;
  if (this.finalY + 100 > this.y) {
  	this.transparency -= 0.005;
  }

  if (this.transparency <= 0) {
  	this.deleteMe = true;
  }
};

Particle.prototype.render = function(ctx, x, y) {
	ctx.fillStyle = this.color;
  ctx.fillRect(x, y, this.width, this.height);
};
function Checkpoint (x, y, id) {
  this.renderMethod = 'self';
  this.x = x;
  this.y = y;
  this.id = id;
  this.active = false;
  this.width = 26;
  this.height = 50;

  this.shinies = [];
  this.allGathered = false;
}

Checkpoint.prototype = new Actor();

Checkpoint.prototype.update = function() {

};

Checkpoint.prototype.render = function(ctx, renderer) {
  var renderX = this.x - renderer.camera.x;
  var renderY = this.y - renderer.camera.y;

  var circleColor = '#00FF00';
  if (!this.active) {
  	circleColor = '#FF0000';
	}

	//Border
  ctx.fillStyle = '#000000';
	ctx.fillRect(renderX, renderY, 18, 32);
	ctx.beginPath();
	ctx.arc(renderX + 9, renderY, 13, 0, 2 * Math.PI, false);
	ctx.fill();

	//Grey
  ctx.fillStyle = '#AAAAAA';
  ctx.fillRect(renderX + 1, renderY + 1, 16, 30);
  ctx.beginPath();
  ctx.arc(renderX + 9, renderY, 12, 0, 2 * Math.PI, false);
  ctx.fill();

  ctx.fillStyle = circleColor;
  ctx.beginPath();
  ctx.arc(renderX + 9, renderY, 8, 0, 2 * Math.PI, false);
  ctx.fill();

  if (this.allGathered && this.shinies && this.shinies.length > 0) {
    ctx.fillStyle = '#000000';
    ctx.beginPath();
    ctx.arc(renderX + 9, renderY, 5, 0, 2 * Math.PI, false);
    ctx.fill();

    ctx.fillStyle = 'yellow';
    ctx.beginPath();
    ctx.arc(renderX + 9, renderY, 4, 0, 2 * Math.PI, false);
    ctx.fill();
  }

};

Checkpoint.prototype.handleCollision = function(hero) {
	this.active = true;
};

Checkpoint.prototype.addShinies = function(shinies) {
  if (!shinies) return;
  this.shinies = shinies;
  for (var i = 0; i < this.shinies.length; i++) {
    this.shinies[i].checkpoint = this;
  }
};

Checkpoint.prototype.checkAllGathered = function(shinies) {
  for (var i = 0; i < this.shinies.length; i++) {
    if (!this.shinies[i].gathered) return;
  }

  this.allGathered = true;
};
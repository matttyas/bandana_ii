function BaseEnemy (startX, y, endX, direction) {
	this.direction = direction || 1;
	this.width = 14;
	this.height = 14;
	this.speed = 2;
	this.y = y;
	this.startX = startX;
	this.endX = endX;
	this.x = this.startX;

	this.renderMethod = 'self';

	if (this.direction < 0) {
		this.targetX = this.startX;
		this.x = this.endX - this.width;
	}

	this.targetX = this.endX;
};

BaseEnemy.prototype = new Actor();

BaseEnemy.prototype.update = function() {
  this.x += this.speed * this.direction;

  if ((this.direction > 0 && this.x + this.width > this.endX) || (this.direction < 0 && this.x < this.startX)) {
  	//this.x = this.targetX;
    this.direction *= -1;

    if (this.targetX == this.startX) {
    	this.targetX = this.endX;
    } else {
    	this.targetX = this.startX;
    }
  }
};

BaseEnemy.prototype.render = function(ctx, renderer) {
	var renderX = this.x - renderer.camera.x;
	var renderY = this.y - renderer.camera.y;

	//Border
	ctx.fillStyle = '#000000';
	ctx.fillRect(renderX, renderY, this.width, this.height);

	//Fill
  ctx.fillStyle = '#CCFFFF';
  ctx.fillRect(renderX + 1, renderY + 1, this.width - 2, this.height - 2);

  //Eye?


  if (this.direction < 0 ) {
  	ctx.fillStyle = '#000000';
  	ctx.fillRect(renderX + 1, renderY + 1, 8, 8);
  	ctx.fillStyle = '#FFFF00';
  	ctx.fillRect(renderX + 1, renderY + 1, 7, 7);
  	ctx.fillStyle = '#FF0000';
  	ctx.fillRect (renderX + 1, renderY + 2, 4, 4);
  } else {
  	ctx.fillStyle = '#000000';
  	ctx.fillRect(renderX + this.width - 9, renderY + 1, 8, 8);
  	ctx.fillStyle = '#FFFF00';
  	ctx.fillRect(renderX + this.width - 8, renderY + 1, 7, 7);
  	ctx.fillStyle = '#FF0000';
  	ctx.fillRect (renderX + this.width - 5, renderY + 2, 4, 4);
  }
};

BaseEnemy.prototype.handleCollision = function(hero) {
  hero.dead = true;
};
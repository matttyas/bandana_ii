function ParticleSystem (genX, genY, width, height, count, engine, particle) {
	this.renderMethod = 'self';
	this.genX = genX || 0;
	this.genY = genY || 0;
	this.width = width || engine.screenWidth;
	this.height = height || engine.screenHeight;
	this.coreParticle = particle || new Particle();
	this.particles = [];
	this.count = count;
  this.renderer = null;
  this.followCamera = false;

	for (var i = 0; i < this.count; i++) {
    this.generate();
	}

};

ParticleSystem.prototype = new Actor();

ParticleSystem.prototype.render = function (ctx, renderer) {
  for (var i = 0; i < this.particles.length; i++) {


    var renderX = this.particles[i].x - renderer.camera.x;
    var renderY = this.particles[i].y - renderer.camera.y;


    ctx.globalAlpha = this.particles[i].transparency || 1;
    this.particles[i].render(ctx, renderX, renderY);
    ctx.globalAlpha = 1;
  };

  if (this.followCamera) {
    this.genX = renderer.camera.x;
    this.genY = renderer.camera.y;
  }
};

ParticleSystem.prototype.setRenderer = function (renderer) {
  this.renderer = renderer;
};

ParticleSystem.prototype.update = function () {
  for (var i = this.particles.length - 1; i >= 0; i--) {
  	var particle = this.particles[i];
  	particle.update();
  	if (particle.deleteMe) {
  		this.particles.splice(i, 1);
  		this.generatePostInit();
  	}
  }
};

//The particle this system will use
ParticleSystem.prototype.setParticle = function (particle) {
  this.coreParticle = particle;
};

//Create new particle within bounds of the system
ParticleSystem.prototype.generate = function() {
  var x = Math.round(Math.random() * this.width);
  var y = Math.round(Math.random() * this.height);
  x = this.genX + x;
  y = this.genY + y;

  var newParticle = this.coreParticle.generate(x, y, 0);


  this.particles.push(newParticle);
};

ParticleSystem.prototype.generatePostInit = function() {
  var x = Math.round(Math.random() * this.width);
  var y = Math.round(this.y + this.height + 50);
  x = this.genX + x;
  y = this.genY + y;

  var newParticle = this.coreParticle.generate(x, y, 0);

  this.particles.push(newParticle);
};

ParticleSystem.prototype.setFollowCamera = function(followCamera) {
  this.followCamera = followCamera;
}
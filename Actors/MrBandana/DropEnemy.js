function DropEnemy (x, y, height) {
  this.x = x;
  this.y = y;
  this.renderMethod = 'self';
  this.width = 20;
  this.height = height;

  //REMOVE THIS !!!!
  //this.height = 100;

  //REMOVE THAT --^

  this.dropSpike = false;
  this.debug = false;
  this.projectilePoints = [{x: this.x + 2, y: this.y + 8}, {x: this.x + this.width - 2, y: this.y + 8}, {x: this.x + this.width / 2, y: this.y + 20}];
  this.baseHeight = 12;
}

DropEnemy.prototype = new Actor();

DropEnemy.prototype.render = function(ctx, renderer) {
  var renderX = this.x - renderer.camera.x;
  var renderY = this.y - renderer.camera.y;

  if (this.debug) {
  	ctx.fillStyle = '#FAAFBE';
  	ctx.fillRect(renderX, renderY, this.width, this.height);
  }
  //Base
  ctx.fillStyle = '#000000';
  ctx.fillRect(renderX, renderY, this.width, this.baseHeight);

  //Spike
  //Border
  renderer.drawTriangle(this.projectilePoints, '#000000', renderer, ctx);


  var pp = this.projectilePoints;

  var inPoints = [ {x: pp[0].x + 1, y: pp[0].y + 1}];
  inPoints.push( {x: pp[1].x - 1, y: pp[1].y + 1});
  inPoints.push({x: pp[2].x, y: pp[2].y - 1});
  renderer.drawTriangle(inPoints, '#FFFFFF', renderer, ctx);

  ctx.fillStyle = '#330099';
  ctx.fillRect(renderX + 1, renderY + 1, this.width - 2, this.baseHeight - 2);
};

DropEnemy.prototype.handleCollision = function(hero) {
  var yDiff = this.y - hero.y;
  if (yDiff < 0) yDiff *= -1;
  if (yDiff < 20) {
    hero.dead = true;
    return;
  }

  if (hero.y > this.projectilePoints[0].y && hero.y < this.projectilePoints[2].y) {
    hero.dead = true;
    return;
  }

  this.dropSpike = true;

};

DropEnemy.prototype.update = function() {
  if (this.dropSpike) {
    var pp = this.projectilePoints;
    for (var i = 0; i < pp.length; i++) {
      pp[i].y += 3;
    }

    if (pp[0].y > this.y + this.height - 5) {
      pp[0].y = this.y + 8;
      pp[1].y = this.y + 8;
      pp[2].y = this.y + 20;
      this.dropSpike = false;
    }
  }
};
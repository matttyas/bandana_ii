function Renderer(ctx, width, height) {
	this.ctx = ctx;
	this.width = width;
	this.height = height;
	this.currentScene = null;
	this.camera = new Camera();
  this.useGradientForBack = true;
  this.gradientColors = ['#663399', '#F88017'];
  //this.gradientColors = ['#000000', '#770000'];
  this.backColor = '#659CEF';
  this.hideScrollingBack = false;

  var canvas = document.getElementById('game');

  this.buffer = document.createElement('canvas');
  this.buffer.width = canvas.width;
  this.buffer.height = canvas.height;
  this.buffer_context = this.buffer.getContext('2d');
}

Renderer.prototype.render = function() {

	this.camera.update();
  this.clearBackground();

  if (this.currentScene && this.currentScene.name == 'Bandana Scene') {

    if (!this.hideScrollingBack) {
      this.drawActor(this.currentScene.levelScrollingBackground);
    }

    if (this.currentScene.particleSystem && this.currentScene.useBackParticleSystem) {
      this.drawActor(this.currentScene.particleSystem, false);
    }

  	for (var i = 0; i < this.currentScene.backPanels.length; i++) {
  		this.drawActor(this.currentScene.backPanels[i], true);
  	}

  	for (var i = 0; i < this.currentScene.deathTraps.length; i++) {
  		this.drawActor(this.currentScene.deathTraps[i], true);
  	}

  	for (var i = 0; i < this.currentScene.platforms.length; i++) {
  		this.drawActor(this.currentScene.platforms[i], true);
  	}

  	for (var i = 0; i < this.currentScene.shinies.length; i++) {
  		this.drawActor(this.currentScene.shinies[i], true);
  	}

  	for (var i = 0; i < this.currentScene.checkpoints.length; i++) {
  		this.drawActor(this.currentScene.checkpoints[i], true);
  	}

  	for (var i = 0; i < this.currentScene.devices.length; i++) {
  		this.drawActor(this.currentScene.devices[i], true);
  	}

  	for (var i = 0; i < this.currentScene.enemies.length; i++) {
  		this.drawActor(this.currentScene.enemies[i], true);
  	}

  	this.drawActor(this.currentScene.hero, true);
  	this.drawActor(this.currentScene.interface, true);

    this.ctx.drawImage(this.buffer, 0, 0);

  	return;
  }

  if (this.currentScene) {
  	for (var i = 0; i < this.currentScene.actors.length; i++) {
  		this.drawActor(this.currentScene.actors[i], true);
  	}
  }

}

Renderer.prototype.clearBackground = function() {
  if (this.useGradientForBack) {
    var grd = this.buffer_context.createLinearGradient(0, 0, 0, this.height);
    grd.addColorStop(0, this.gradientColors[0]);
    grd.addColorStop(1, this.gradientColors[1]);
    this.buffer_context.fillStyle = grd;

  } else {
    this.buffer_context.fillStyle = this.backColor;
  }

  this.buffer_context.fillRect(0,0, this.width, this.height);

}

Renderer.prototype.drawActor = function(actor, blur) {

	if (actor.renderMethod == 'self') {
		actor.render(this.buffer_context, this, blur);
	}
	else if (actor.renderMethod == 'square') {
		this.buffer_context.fillStyle = actor.color || '#000000';
		this.buffer_context.fillRect(actor.x, actor.y, actor.width, actor.height);
	} else if (actor.renderMethod == 'circle') {

		if (actor.style && actor.style == 'stroke') {
			this.buffer_context.lineWidth = actor.lineWidth || 1;
			this.buffer_context.strokeStyle = actor.color || '#000000';
			this.buffer_context.beginPath();
		  this.buffer_context.arc(actor.x, actor.y, actor.width, 0, 2 * Math.PI, false);
		  this.buffer_context.stroke();
		} else {
			this.buffer_context.fillStyle = actor.color || '#000000';
			this.buffer_context.beginPath();
		  this.buffer_context.arc(actor.x, actor.y, actor.width, 0, 2 * Math.PI, false);
		  this.buffer_context.fill();
		}
	}

}

Renderer.prototype.setCurrentScene = function(scene) {
	this.currentScene = scene;
};

Renderer.prototype.blur = function(actor) {
	var oldX = actor.x;
	var oldY = actor.y;

	actor.x = actor.x - this.camera.movement.x / 2;
	actor.y = actor.y - this.camera.movement.y / 2;

	this.buffer_context.globalAlpha = 0.5;

	this.drawActor(actor, false);

	this.buffer_context.globalAlpha = 1;
	actor.x = oldX;
	actor.y = oldY;
}

Renderer.prototype.setUseGradientForBack = function (useGradientForBack) {
  this.useGradientForBack = useGradientForBack;
};

Renderer.prototype.setGradientColors = function (colors) {
  this.gradientColors = colors;
};

Renderer.prototype.useBackColor = function(color) {
  this.useGradientForBack = false;
  if (color) this.backColor = color;
};

Renderer.prototype.setHideScrollingBack = function(hideScrollingBack) {
  this.hideScrollingBack = hideScrollingBack;
};

//Naughty. But oh well :D
Renderer.prototype.useScrollingBack = function(scrollingBack) {
  this.currentScene.levelScrollingBackground = scrollingBack;
};

Renderer.prototype.setUseBackParticleSystem = function (useBackParticleSystem) {
  this.currentScene.useBackParticleSystem = useBackParticleSystem;
};

Renderer.prototype.drawTriangle = function(triangle, fillStyle) {
  this.buffer_context.fillStyle = fillStyle;
  this.buffer_context.beginPath();
  this.buffer_context.moveTo(triangle[0].x - this.camera.x, triangle[0].y - this.camera.y);
  for (var i = 0; i < triangle.length; i++) {
    if (i != 0) this.buffer_context.lineTo(triangle[i].x - this.camera.x, triangle[i].y - this.camera.y);
  }
  this.buffer_context.lineTo(triangle[0].x - this.camera.x, triangle[0].y - this.camera.y);
  this.buffer_context.fill();
  this.buffer_context.closePath();
};
function World () {
  this.world = null;
}

//Returns the whole world as an object. He's got the whole world in his hands...

//For now the whole world is declared in the function below. It basically goes from the start to the end of the game.
//I think it
World.prototype.buildWorld = function(engine, currentScene) {
	if (this.world) return this.world;

	this.world = {};
	this.world.objects = [];
	this.world.platforms = [];
  this.world.checkpoints = [];
  this.world.deathTraps = [];
  this.world.backPanels = [];
  this.world.shinies = [];
  this.world.devices = [];
  this.world.enemies = [];
  //Tree background
  var darkWood = '#6F4E37';
  var caveBack = '#777777';


  //Tutorial Zone
  this.buildTutorial();

  this.world.platforms.push(new Platform(0, 316, 800, 165));


  //First vertical tunnel
  this.world.backPanels.push(new BackPanel(0, 320, 2470, 1000, [caveBack]));
  this.world.platforms.push(new Platform(700, 280, 140, 201));
  this.world.platforms.push(new Platform(825, 480, 380, 300));
  this.world.platforms.push(new Platform(0, 1120, 850, 800));

  //The pit

  this.world.platforms.push(new Platform(0, 610, 140, 10, [darkWood]));

  this.world.shinies.push(new Shiny(110,594, 'PIT'));
  this.world.shinies.push(new Shiny(102,580, 'PIT'));
  this.world.shinies.push(new Shiny(94,594, 'PIT'));

  this.world.backPanels.push(new BackPanel(0, 530, 140, 10, ['#806517']));
  this.world.platforms.push(new Platform(130, 530, 10, 81, [darkWood]));
  this.world.backPanels.push(new BackPanel(130, 610, 10, 131, ['#806517']));


  this.world.platforms.push(new Platform(0, 740, 200, 10, [darkWood]));
  this.world.devices.push(new Ladder(160, 740, 140, [darkWood, caveBack]));
  this.world.backPanels.push(new BackPanel(185, 740, 10, 141, ['#806517']));

  this.world.enemies.push(new BaseEnemy(300, 666, 400));
  this.world.platforms.push(new Platform(300, 680, 100, 10));

  this.world.enemies.push(new BaseEnemy(470, 626, 570, -1));
  this.world.platforms.push(new Platform(470, 640, 100, 10));

  this.world.enemies.push(new BaseEnemy(250, 576, 350));
  this.world.platforms.push(new Platform(250, 590, 100, 10));

  this.world.platforms.push(new Platform(650, 550, 120, 10));
  this.world.shinies.push(new Shiny(675, 534, 'PIT'));
  this.world.shinies.push(new Shiny(735, 534, 'PIT'));

  this.world.enemies.push(new BaseEnemy(90, 1006, 320));
  this.world.platforms.push(new Platform(90, 1020, 230, 10, [darkWood]));
  this.world.backPanels.push(new BackPanel(90, 1029, 10, 160, ['#806517']));
  this.world.backPanels.push(new BackPanel(200, 1029, 10, 160, ['#806517']));
  this.world.backPanels.push(new BackPanel(310, 1029, 10, 160, ['#806517']));
  this.world.shinies.push(new Shiny(148,1004, 'PIT'));

  this.world.platforms.push(new Platform(130, 880, 130, 10, [darkWood]));
  this.world.backPanels.push(new BackPanel(130, 880, 10, 150, ['#806517']));
  this.world.backPanels.push(new BackPanel(250, 880, 10, 150, ['#806517']));

  this.world.platforms.push(new Platform(420, 1000, 100, 10, [darkWood]));
  this.world.backPanels.push(new BackPanel(420, 1009, 10, 120, ['#806517']));
  this.world.backPanels.push(new BackPanel(510, 1009, 10, 120, ['#806517']));

  this.world.platforms.push(new Platform(510, 1050, 100, 10, [darkWood]));
  this.world.backPanels.push(new BackPanel(595, 1059, 10, 70, ['#806517']));

  this.world.platforms.push(new Platform(672, 930, 150, 10, [darkWood]));
  this.world.backPanels.push(new BackPanel(682, 939, 10, 200, ['#806517']));

  this.world.shinies.push(new Shiny(806,970, 'PIT'));
  this.world.shinies.push(new Shiny(806,990, 'PIT'));
  this.world.shinies.push(new Shiny(806,1010, 'PIT'));


  this.world.platforms.push(new Platform(822, 930, 300, 1000));
  this.world.checkpoints.push(new Checkpoint(872, 897, 'PIT'));
  this.world.platforms.push(new Platform(1020, 850, 1450, 1000));


  //First Cave

  this.world.deathTraps.push(new DeathTrap(0,1100, 820, 20));


  this.world.platforms.push(new Platform(1260, 480, 875, 300));


  this.world.platforms.push(new Platform(1100, 280, 360, 55));
  //this.world.enemies.push(new BaseEnemy(1100, 265, 1460));


  this.world.platforms.push(new Platform(1670, 240, 800, 400));
  this.world.backPanels.push(new BackPanel(840, 280, 1000, 200, [caveBack]));

  //Trees

  this.addBigTree(100, -580, true);
  this.addTree(800, -270);

  this.world.platforms.push(new Platform(855, -235, 105, 10, ['#349C17']));

  this.addTree(1150, -270);
  this.addTree(800, -270);
  this.addTree(1710, -310);
  //Big tree

  this.world.platforms.push(new Platform(500, -205, 100, 10, [darkWood]));
  this.world.platforms.push(new Platform(350, -285, 100, 10, [darkWood]));
  this.world.platforms.push(new Platform(200, -365, 100, 10, [darkWood]));
  this.world.platforms.push(new Platform(350, -445, 100, 10, [darkWood]));
  //this.world.platforms.push(new Platform(100, -480, 10, 400, [darkWood]));
  //dDaaadadathis.world.platforms.push(new Platform(100, -80, 600, 10, [darkWood]));

//Top of big tree

  this.world.platforms.push(new Platform(-100, -490, 450, 10, ['#349C17']));
  this.world.platforms.push(new Platform(-100, -980, 10, 500, ['#349C17']))
  this.world.platforms.push(new Platform(-100, -980, 900, 10, ['#349C17']))
  this.world.platforms.push(new Platform(900, -980, 10, 500, ['#349C17']))
  this.world.platforms.push(new Platform(450, -490, 450, 10, ['#349C17']))


  //second tree

  this.world.platforms.push(new Platform(800, 60, 210, 10, [darkWood]));

  //third tree

  this.world.backPanels.push(new BackPanel(1010, 70, 145, 30, ['#966F33']));
  this.world.platforms.push(new Platform(1010, 99, 145, 10, [darkWood]));
  this.world.platforms.push(new Platform(1150, 99, 10, 100, [darkWood]));
  this.world.platforms.push(new Platform(1150, 0, 100, 10, [darkWood]));
  this.world.platforms.push(new Platform(1310, -70, 50, 10, [darkWood]));
  this.world.platforms.push(new Platform(1360, -150, 50, 10, ['#349C17']));
  this.world.platforms.push(new Platform(1205, -235, 105, 10, ['#349C17']));

  this.world.shinies.push(new Shiny(860, 40, 'A'));
  this.world.shinies.push(new Shiny(900, 40, 'A'));
  this.world.shinies.push(new Shiny(940, 40, 'A'));

  this.world.shinies.push(new Shiny(1210, 260, 'A'));
  this.world.shinies.push(new Shiny(1250, 260, 'A'));
  this.world.shinies.push(new Shiny(1290, 260, 'A'));

  //Fourth tree

  this.world.backPanels.push(new BackPanel(1620, 0, 90, 40, ['#966F33']));
  this.world.platforms.push(new Platform(1620, 39, 90, 10, [darkWood]));

  //this.world.deathTraps.push(new DeathTrap(840,460, 250, 20));
  this.world.deathTraps.push(new DeathTrap(1670,220, 100, 20));

  this.world.shinies.push(new Shiny(275, 302, 'A'));
  this.world.shinies.push(new Shiny(315, 302, 'A'));
  this.world.shinies.push(new Shiny(355, 302, 'A'));

  this.world.shinies.push(new Shiny(435, 302, 'A'));
  this.world.shinies.push(new Shiny(475, 302, 'A'));
  this.world.shinies.push(new Shiny(515, 302, 'A'));

	//Checkpoints

  this.world.checkpoints.push(new Checkpoint(390, 284, 'A'));
  this.world.checkpoints.push(new Checkpoint(384, -477, 'B'));
  this.world.checkpoints.push(new Checkpoint(2200, 208, 'C'));  this.world.checkpoints.push(new Checkpoint(2200, 208, 'C'));

  //FROM NOW ONWARD GOING FROM LEFT TO RIGHT

  this.world.platforms.push(new Platform(2470, 920, 750, 350));
  this.world.platforms.push(new Platform(3320, 920, 750, 350));
  this.world.deathTraps.push(new DeathTrap(2470,900, 750, 20));
  this.world.deathTraps.push(new DeathTrap(3320,900, 750, 20));

  this.addBigTree(2550, 20, true);
  this.world.platforms.push(new Platform(2650, 320, 100, 10, [darkWood]));
  this.world.platforms.push(new Platform(2950, 395, 100, 10, [darkWood]));

  this.world.platforms.push(new Platform(3050, 395, 10, 405, [darkWood]));
  this.world.platforms.push(new Platform(3050, 795, 100, 10, [darkWood]));
  //this.world.platforms.push(new Platform(2950, 525, 100, 10, [darkWood]));
  this.world.platforms.push(new Platform(2650, 600, 100, 10, [darkWood]));
  this.world.devices.push(new Ladder(2693, 600, 200, [darkWood, '#966F33']));

  //this.world.platforms.push(new Platform(2950, 675, 100, 10, [darkWood]));



  //The canyon pit
  this.world.backPanels.push(new BackPanel(2470, 920, 1498,1500, [caveBack]));
  this.world.platforms.push(new Platform(2470, 1269, 450, 800)); //Left wall
  this.world.platforms.push(new Platform(2020, 1669, 2600, 700)); //Floor
  this.world.platforms.push(new Platform(3623, 1269, 900, 800)); //Right wall
  this.world.deathTraps.push(new DeathTrap(2920,1649, 700, 20)); //Floor spikes

  var pitMover = new Platform(3220, 1600, 100, 10);
  pitMover.setCanMove(true);
  this.world.platforms.push(pitMover);

  this.world.platforms.push(new Platform(2950, 1550, 175, 10));
  this.world.platforms.push(new Platform(2950, 1301, 10, 250));
  this.world.shinies.push(new Shiny(2980, 1534, 'C'));
  this.world.shinies.push(new Shiny(3086, 1534, 'C'));

  this.world.platforms.push(new Platform(3420, 1550, 175, 10));
  this.world.platforms.push(new Platform(3585, 1301, 10, 250));
  this.world.shinies.push(new Shiny(3450, 1534, 'C'));
  this.world.shinies.push(new Shiny(3546, 1534, 'C'));




  var pitSwitches = [ new Switch(3000, 1325, pitMover), new Switch(3515, 1325, pitMover) ];

  pitSwitches[1].on = true;

  this.world.devices.push(pitSwitches[0]);
  this.world.devices.push(pitSwitches[1]);

  pitMover.setSwitches(pitSwitches);
  pitMover.setTargetLocation(3220,-180);
  pitMover.setSpeed(7);
  pitMover.setRocket(true);

  //Platforms left of top of big tree
  this.world.platforms.push(new Platform(2750, -350, 200, 10, ['#349C17']))
  this.world.platforms.push(new Platform(2550, -150, 200, 10, ['#349C17']))
  this.world.platforms.push(new Platform(2350, 50, 200, 10, ['#349C17']))

  //Top of big tree accessed by pit lift

  this.world.backPanels.push(new BackPanel(2950, -781, 600, 401, ['#CCCCCC']));
  this.world.platforms.push(new Platform(3540, -781, 10, 401, ['#000000']));
  this.world.platforms.push(new Platform(2950, -390, 200, 10, ['#000000']));
  this.world.platforms.push(new Platform(2950, -781, 600, 10, ['#000000']));


  this.world.platforms.push(new Platform(3355, -390, 195, 10, ['#000000']));
  this.world.platforms.push(new Platform(3450, -470, 100, 10, ['#000000']));
  this.world.platforms.push(new Platform(2955, -550, 395, 10, ['#000000']));
  this.world.platforms.push(new Platform(2950, -630, 150, 10, ['#000000']));
  this.world.platforms.push(new Platform(2950, -629, 10, 248, ['#000000']));


  this.addBigTree(3390, 20, true);
  this.world.platforms.push(new Platform(3390, 795, 100, 10, [darkWood]));

  this.world.enemies.push(new BaseEnemy(3740, 780, 3990));
  this.world.platforms.push(new Platform(3740, 795, 250, 10, [darkWood]));
  this.world.platforms.push(new Platform(3790, 635, 150, 10, [darkWood]));
  this.world.platforms.push(new Platform(3490, 585, 150, 10, [darkWood]));
  this.world.enemies.push(new BaseEnemy(3515, 420, 3865));
  this.world.platforms.push(new Platform(3515, 435, 400, 10, [darkWood]));
  this.world.devices.push(new Ladder(3558, 435, 150, [darkWood, '#966F33']));
  this.world.shinies.push(new Shiny(3560, 419, 'C'));
  this.world.shinies.push(new Shiny(3875, 419, 'C'));
  this.world.platforms.push(new Platform(3800, 285, 150, 10, [darkWood]));
  this.world.devices.push(new Ladder(3820, 285, 150, [darkWood, '#966F33']));

  //this.world.backPanels.push(new BackPanel(3250, 410, 140, 50, ['#966F33']));
  //this.world.platforms.push(new BackPanel(3250, 459, 140, 10, [darkWood]));

  //this.world.backPanels.push(new BackPanel(3250, 675, 140, 50, ['#966F33']));
  //this.world.platforms.push(new BackPanel(3250, 724, 140, 10, [darkWood]));

  this.world.platforms.push(new Platform(4070, 240, 800, 101));
  this.world.backPanels.push(new BackPanel(4070, 240, 800, 600, [caveBack]));
  //this.world.platforms.push(new Platform(4300, 480, 800, 300));
  this.world.platforms.push(new Platform(4070, 720, 800, 600));
  this.world.platforms.push(new Platform(4270, 570, 600, 151));
  this.world.platforms.push(new Platform(4270, 320, 10, 201));
  var door1 = new Door(4270, 511, 58);
  this.world.devices.push(door1);

  this.world.checkpoints.push(new Checkpoint(4300, 208, 'D'));

  this.world.platforms.push(new Platform(4475, 510, 100, 10));
  var doorSwitches = [new Switch(4515, 420, door1)];

  this.world.devices.push(doorSwitches[0]);
  door1.setSwitches(doorSwitches);

  this.world.platforms.push(new Platform(4770, 320, 250, 200));
  this.world.backPanels.push(new BackPanel(4870, 519, 300, 100, [caveBack]));
  this.world.platforms.push(new Platform(4870, 570, 800, 800));
  this.world.platforms.push(new Platform(5020, 460, 150, 60));

  this.addTree(5194, 30);

  this.world.platforms.push(new Platform(5430, 370, 530, 150));
  var door2 = new Door(5430, 520, 49);
  this.world.devices.push(door2);
  door2.setSwitches(doorSwitches);
  doorSwitches[0].addTarget(door2);
  door2.openDoor();

  this.world.platforms.push(new Platform(5680, 316, 280, 56));
  this.world.platforms.push(new Platform(5850, 500, 110, 160));

  //this.world.platforms.push(new Platform(5900, 660, 110, 160));


  this.world.platforms.push(new Platform(5670, 570, 110, 90));
  this.world.shinies.push(new Shiny(5700, 904, 'D'));
  this.world.shinies.push(new Shiny(5921, 904, 'D'));
  this.world.platforms.push(new Platform(5670, 920, 1500, 594));
  this.world.backPanels.push(new BackPanel(5430, 519, 550, 500, [caveBack]));
  this.world.platforms.push(new Platform(5960, 520, 1000, 900));
  this.world.deathTraps.push(new DeathTrap(5960,500, 430, 20));
  this.world.enemies.push(new BaseEnemy(5670, 905, 5960));


  var movePlatform = new Platform(5780, 880, 70, 10);
  movePlatform.setCanMove(true);

  movePlatform.setTargetLocation(5780,650);
  movePlatform.setSpeed(3);
  movePlatform.setRocket(true);
  var newSwitches = [new Switch(5900, 700, movePlatform)];
  this.world.devices.push(newSwitches[0]);
  movePlatform.setSwitches(newSwitches);
  this.world.platforms.push(movePlatform);

  var spikePlatform = new Platform(6126, 520, 100, 10)
  spikePlatform.setCanMove(true);
  spikePlatform.setTargetLocation(6126, 321);
  spikePlatform.setSpeed(3);
  spikePlatform.setSwitches(newSwitches);
  newSwitches[0].addTarget(spikePlatform);

  this.world.platforms.push(spikePlatform);

  this.world.shinies.push(new Shiny(6167, 341, 'D'));
  this.world.shinies.push(new Shiny(6167, 391, 'D'));
  this.world.shinies.push(new Shiny(6167, 441, 'D'));

  this.world.platforms.push(new Platform(6392, 316, 1000, 1198));

  this.world.platforms.push(new Platform(6550, 277, 200, 40, ['#BBBBBB']));
  this.world.backPanels.push(new BackPanel(6580, -223, 140, 501, ['#DDDDDD']));
  this.world.platforms.push(new Platform(6650, 128, 70, 10, ['#BBBBBB']));
  this.world.devices.push(new Ladder(6680, 128, 150, ['#BBBBBB', '#DDDDDD']));
  this.world.platforms.push(new Platform(6650, -23, 70, 10, ['#BBBBBB']));
  this.world.platforms.push(new Platform(6550, -262, 200, 40, ['#BBBBBB']));
  this.world.platforms.push(new Platform(6710, -223, 10, 70, ['#BBBBBB']));

  this.world.platforms.push(new Platform(6950, 277, 200, 40, ['#BBBBBB']));
  this.world.backPanels.push(new BackPanel(6980, -72, 140, 350, ['#DDDDDD']));
  this.world.platforms.push(new Platform(6920, 52, 70, 10, ['#806517']));
  this.world.platforms.push(new Platform(6980, 52, 10, 40, ['#806517']));
  this.world.platforms.push(new Platform(7020, -87, 100, 20, ['#BBBBBB']));
  this.world.platforms.push(new Platform(7020, -106, 50, 20, ['#BBBBBB']));
  this.world.platforms.push(new Platform(6980, -115, 40, 48, ['#BBBBBB']));

  this.world.backPanels.push(new BackPanel(7392, 316, 1000, 298, [caveBack]));
  var gradPanel = new BackPanel(7832, 613, 83, 500, [caveBack]);
  gradPanel.setGradient(caveBack, '#000000');
  this.world.backPanels.push(gradPanel);
  this.world.platforms.push(new Platform(7392, 391, 75, 75));
  this.world.platforms.push(new Platform(7392, 465, 149, 149));
  this.world.platforms.push(new Platform(7392, 539, 224, 574));
  this.world.platforms.push(new Platform(7466, 465, 75, 75));
  this.world.platforms.push(new Platform(7540, 539, 75, 75));
  this.world.platforms.push(new Platform(7614, 613, 225, 500));
  this.world.platforms.push(new Platform(7914, 613, 450, 500));

  this.world.platforms.push(new Platform(7571, 316, 613, 75));
  this.world.backPanels.push(new BackPanel(7571, 391, 10, 200, ['#806517']));
  this.world.backPanels.push(new BackPanel(8174, 391, 10, 200, ['#806517']));

  this.world.backPanels.push(new BackPanel(7832, 613, 83, 10, ['#806517']));

  var gradPanelTrigger = new BackPanelTrigger(7839, 713, 83, 20, engine.renderer);
  gradPanelTrigger.setUseGradient(true);
  gradPanelTrigger.setColors(['#663399', '#F88017'])
  gradPanelTrigger.setHideScrollingBack(false);
  gradPanelTrigger.setScrollingBack(currentScene.outScrollingBackground);
  gradPanelTrigger.setUseBackParticleSystem(false);
  this.world.devices.push(gradPanelTrigger);


  var blackPanelTrigger = new BackPanelTrigger(7839, 733, 83, 20, engine.renderer);
  blackPanelTrigger.setColors(['#000000', '#550000']);
  blackPanelTrigger.setUseGradient(true);
  blackPanelTrigger.setHideScrollingBack(false);
  blackPanelTrigger.setScrollingBack(currentScene.inScrollingBackground);
  blackPanelTrigger.setUseBackParticleSystem(true);

  this.world.devices.push(blackPanelTrigger);

  this.world.platforms.push(new Platform(8139, 539, 75, 75));
  this.world.platforms.push(new Platform(8214, 465, 150, 149));
  this.world.platforms.push(new Platform(8289, 391, 75, 75));
  this.world.platforms.push(new Platform(8364, 316, 1000, 1100));
  this.world.platforms.push(new Platform(9364, 316, 1000, 1100));
  this.world.platforms.push(new Platform(10364, 316, 1000, 1100));
  //THE CAVES BELOW



  this.world.checkpoints.push(new Checkpoint(7875, 1581, 'E'));

  this.world.checkpoints.push(new Checkpoint(5788, 1781, 'F'));
  this.world.platforms.push(new Platform(4070, 1520, 400, 900));
  this.world.platforms.push(new Platform(4070, 2320, 400, 893));

  this.world.platforms.push(new Platform(4870, 3013, 800, 200));
  this.world.deathTraps.push(new DeathTrap(4870, 2993, 800, 20));


  var controlSwitches = [ new Switch(5390, 2453, null) ];
  this.world.devices.push(controlSwitches[0]);
  this.world.devices.push(new Ladder(5430, 2483, 130, ['#AAAAAA', '#580058']));
  this.world.platforms.push(new Platform(5370, 2363, 300, 10));
  this.world.platforms.push(new Platform(5370, 2483, 300, 10));
  this.world.platforms.push(new Platform(5370, 2613, 300, 10));
  this.world.platforms.push(new Platform(5370, 2363, 10, 251));
  this.world.backPanels.push(new BackPanel(5370, 2363, 315, 250, ['#580058']));
  var controlDoor = new Door(5681, 2514, 99);
  this.world.devices.push(controlDoor);


  this.world.platforms.push(new Platform(4620, 1823, 600, 10)); //Conveyor belt
  this.world.devices.push(new Ladder(4870, 1823, 150, ['#AAAAAA']));
  this.world.platforms.push(new Platform(4820, 1973, 200, 10));
  this.world.devices.push(new Ladder(4970, 1973, 80, ['#AAAAAA']));
  this.world.devices.push(new Ladder(4970, 2173, 100, ['#AAAAAA']));
  this.world.platforms.push(new Platform(4930, 2273, 100, 10));
  this.world.platforms.push(new Platform(5160, 2323, 100, 10));
  this.world.platforms.push(new Platform(5160, 2653, 100, 10));
  this.world.platforms.push(new Platform(5360, 2743, 100, 10));
  this.world.platforms.push(new Platform(5540, 2673, 100, 10));

  var controlAccessSwitches = [ new Switch(5580, 2638, controlDoor) ];
  this.world.devices.push(controlAccessSwitches[0]);
  controlDoor.setSwitches(controlAccessSwitches);

  this.world.platforms.push(new Platform(5220, 1823, 100, 10));
  this.world.platforms.push(new Platform(5570, 1823, 100, 10));

  this.world.platforms.push(new Platform(5670, 1513, 322, 200));
  this.world.platforms.push(new Platform(5670, 1813, 322, 200));
  this.world.platforms.push(new Platform(5670, 2012, 922, 200));
  this.world.platforms.push(new Platform(5670, 2211, 522, 303));
  this.world.platforms.push(new Platform(5670, 2613, 823, 600));
  this.world.platforms.push(new Platform(6292, 2312, 500, 201));

  this.world.platforms.push(new Platform(6092, 1613, 800, 301));

  var tunnelDoor1 = new Door(5887, 1713, 100);
  this.world.devices.push(tunnelDoor1);

  var tunnelDoorSwitches = [ new Switch(8342, 2013, tunnelDoor1), new Switch(7137, 2313, tunnelDoor1), new Switch(7954, 2993, tunnelDoor1) ];
  this.world.devices.push(tunnelDoorSwitches[0]);
  this.world.devices.push(tunnelDoorSwitches[1]);
  this.world.devices.push(tunnelDoorSwitches[2]);

  tunnelDoor1.setSwitches(tunnelDoorSwitches);
  tunnelDoor1.openDoor();
  this.world.enemies.push(new DropEnemy(6892, 1515, 400));
  this.world.enemies.push(new DropEnemy(6972, 1515, 400));
  this.world.platforms.push(new Platform(6692, 2012, 100, 200));

  this.world.platforms.push(new Platform(6992, 1613, 400, 200));
  this.world.platforms.push(new Platform(6692, 1913, 600, 100));
  this.world.platforms.push(new Platform(6692, 2413, 600, 100));
  this.world.deathTraps.push(new DeathTrap(6791, 2393, 100, 20));
  this.world.platforms.push(new Platform(6892, 2113, 10, 301));
  this.world.shinies.push(new Shiny(6893, 2060, 'E'));
  this.world.deathTraps.push(new DeathTrap(6901, 2393, 200, 20));
  this.world.platforms.push(new Platform(6992, 2013, 10, 201));
  this.world.shinies.push(new Shiny(6993, 2264, 'E'));
  this.world.shinies.push(new Shiny(6993, 2294, 'E'));
  this.world.shinies.push(new Shiny(7093, 2060, 'E'));

  this.world.deathTraps.push(new DeathTrap(7091, 2393, 100, 20));
  this.world.platforms.push(new Platform(7092, 2113, 10, 301));
  this.world.platforms.push(new Platform(6492, 2613, 300, 600));
  this.world.platforms.push(new Platform(6791, 2693, 90, 10, [darkWood]));
  this.world.platforms.push(new Platform(7002, 2773, 90, 10, [darkWood]));
  this.world.devices.push(new Ladder(7037, 2773, 300, [darkWood, caveBack]));
  this.world.platforms.push(new Platform(7002, 3073, 90, 10, [darkWood]));
  this.world.platforms.push(new Platform(6791, 3153, 90, 10, [darkWood]));
  this.world.devices.push(new Ladder(6831, 3153, 400, [darkWood, caveBack]));
  this.world.platforms.push(new Platform(6591, 3553, 700, 10, [darkWood]));
  this.world.platforms.push(new Platform(6591, 3213, 10, 341, [darkWood]));
  this.world.platforms.push(new Platform(7281, 3213, 10, 271, [darkWood]));
  var observeDoor1 = new Door(7281, 3484, 69);
  this.world.devices.push(observeDoor1);

  this.world.platforms.push(new Platform(7092, 2613, 500, 600));

  this.world.platforms.push(new Platform(7192, 2012, 100, 426));
  this.world.enemies.push(new DropEnemy(7292, 1515, 100));
  this.world.platforms.push(new Platform(7392, 1613, 500, 500));
  this.world.platforms.push(new Platform(7892, 1613, 500, 350));
  this.world.platforms.push(new Platform(7392, 2213, 500, 300));
  this.world.platforms.push(new Platform(7892, 2363, 500, 350));
  this.world.deathTraps.push(new DeathTrap(7891,2343, 500, 20));

  this.world.shinies.push(new Shiny(8026, 2227, 'E'));
  this.world.shinies.push(new Shiny(8246, 2227, 'E'));
  this.world.platforms.push(new Platform(8016,2243, 250, 10));
  this.world.enemies.push(new BaseEnemy(8016, 2229, 8266));
  this.world.enemies.push(new BaseEnemy(8016, 2229, 8266, -1));

  this.world.platforms.push(new Platform(7592, 2828, 300, 10));
  this.world.deathTraps.push(new DeathTrap(7592, 2808, 100, 20));
  this.world.platforms.push(new Platform(7692, 2943, 300, 10));
  this.world.deathTraps.push(new DeathTrap(7892, 2923, 100, 20));
  this.world.platforms.push(new Platform(7692, 2363, 200, 350));
  this.world.deathTraps.push(new DeathTrap(7592, 3043, 100, 20));

  this.world.platforms.push(new Platform(7592, 3063, 400, 150));
  this.world.platforms.push(new Platform(7992, 2712, 400, 501));

  //First room right
  this.world.platforms.push(new Platform(8392, 1613, 500, 501));
  this.world.platforms.push(new Platform(8392, 2113, 500, 500));
  this.world.platforms.push(new Platform(8891, 2614, 1550, 500));

  this.world.checkpoints.push(new Checkpoint(10677, 1982, 'F'));
  this.world.platforms.push(new Platform(10441, 2014, 500, 1100));

  this.world.platforms.push(new Platform(9341, 2114 , 100, 501))
  this.world.platforms.push(new Platform(9891, 2114 , 100, 501))
  this.world.platforms.push(new Platform(11391, 1514, 500, 1250));
  this.world.platforms.push(new Platform(11391, 2763, 1000, 351));

  this.world.platforms.push(new Platform(10441, 5114, 1000, 1000));

  //DO NOT PUT OBJECTS AFTER THIS!!!!
  this.world.objects = this.world.objects.concat(this.world.backPanels);
  this.world.objects = this.world.objects.concat(this.world.platforms);
  this.world.objects = this.world.objects.concat(this.world.shinies);
  this.world.objects = this.world.objects.concat(this.world.checkpoints);
  this.world.objects = this.world.objects.concat(this.world.deathTraps);
  this.world.objects = this.world.objects.concat(this.world.enemies);
  this.calculateDimensions();
  return this.world;
};

World.prototype.addTree = function(x, y) {
  this.world.backPanels.push(new BackPanel(x - 50, y, 310, 200, ['#347C17']));
  this.world.backPanels.push(new BackPanel(x, y + 199, 210, 352, ['#966F33']));
};

World.prototype.addBigTree = function(x, y, backPanel) {
  this.world.backPanels.push(new BackPanel(x, y + 100, 600, 800, ['#966F33']));
  this.world.backPanels.push(new BackPanel(x - 200, y - 400, 1000, 500, ['#347C17']));
};


//WHEN WE LAUNCH WE CAN HARD CODE THIS
World.prototype.calculateDimensions = function() {
  var left = 0;
  var right = 0;
  var top = 0;
  var bottom = 0;

  for (var i = 0; i < this.world.objects.length; i++) {
    var object = this.world.objects[i];
    if (object.x < left) left = object.x;
    if (object.x + object.width > right) right = object.x + object.width;
    if (object.y < top) top = object.y;
    if (object.y + object.height > bottom) bottom = object.y + object.height;
  }

  this.world.x = left;
  this.world.width = right - left;
  this.world.y = top;
  this.world.height = bottom - top;
};

World.prototype.buildTutorial = function() {
  this.world.platforms.push(new Platform(-2400, 316, 200, 600));
  this.world.platforms.push(new Platform(-2210, 400, 120, 600));
  this.world.checkpoints.push(new Checkpoint(-2380, 284, 'TUT'));
  this.world.shinies.push(new Shiny(-2160, 220, 'TUT'));
  this.world.platforms.push(new Platform(-2100, 316, 500, 600));
  this.world.shinies.push(new Shiny(-2056, 300, 'TUT'));
  this.world.platforms.push(new Platform(-2000, 236, 100, 101));
  this.world.shinies.push(new Shiny(-1956, 220, 'TUT'));
  this.world.platforms.push(new Platform(-1900, 156, 300, 201));
  this.world.shinies.push(new Shiny(-1805.5, 140, 'TUT'));
  this.world.shinies.push(new Shiny(-1755.5, 140, 'TUT'));
  this.world.shinies.push(new Shiny(-1705.5, 140, 'TUT'));

  this.world.platforms.push(new Platform(-1600, 316, 800, 600));
  this.world.platforms.push(new Platform(-1600, 236, 100, 201));
  this.world.shinies.push(new Shiny(-1476, 55, 'TUT'));
  this.world.platforms.push(new Platform(-1350, 156, 300, 201));
  this.world.shinies.push(new Shiny(-1165, 65, 'TUT'));
  this.world.platforms.push(new Platform(-1150, 16, 300, 301));
  this.world.platforms.push(new Platform(-800, 316, 800, 1500));

  this.world.checkpoints.push(new Checkpoint(-1005, -16, 'TUT2'));
};
function BandanaChunker () {
	this.chunkWidth = 2000;
	this.chunkHeight = 2000;
	this.chunks = null;
	this.oldY = -1000;
	this.oldX = -1000;
}

//Returns a world object divided up into separate component chunks
BandanaChunker.prototype.chunk = function (world) {
	var chunks = {};

	var chunksLeft = Math.floor(world.x / this.chunkWidth);
	var chunksTop = Math.floor(world.y / this.chunkHeight);

	var xChunks = world.width / this.chunkWidth;
	var yChunks = world.height / this.chunkHeight;

	for (var i = chunksLeft; i <= xChunks + 1; i++) {
		chunks[i] = {};
		for (var j = chunksTop; j <= yChunks + 1; j++) {
			chunks[i][j] = {};
			console.log(i + ', ' + j);
		}
	}

	console.log(chunks);
	//Now have 2d array of chunks representing world ( divided by 1000)

	for (prop in world) {

		//Take array and put an instance of it in each chunk, containing all objects from array within bounds of chunk
		if (world[prop] instanceof Array) {
			for (var i = 0; i < world[prop].length; i++) {

				var object = world[prop][i];

				var x = Math.floor(object.x / this.chunkWidth);
				var y = Math.floor(object.y / this.chunkHeight);

				if ( chunks[x] && chunks[x][y] && chunks[x][y][prop]) {
					chunks[x][y][prop].push(object);
				} else {
					console.log(x);
					console.log(y);
					chunks[x][y][prop] = [];
					chunks[x][y][prop].push(object);
				}

			}
		}
	}

	this.chunks = chunks;

	return this.chunks;

};

BandanaChunker.prototype.getClosestChunks = function(hero) {
	var closestChunks = [];
	var x = Math.floor(hero.x / this.chunkWidth);
	var y = Math.floor(hero.y / this.chunkHeight);

	if (this.oldX == x && this.oldY == y) {
		return false; //No need to rechunk, player in same chunk as before
	}

	this.oldX = x;
	this.oldY = y;

	if (this.chunks[x - 1] && this.chunks[x - 1][y - 1]) closestChunks.push(this.chunks[x - 1][y - 1]);
	if (this.chunks[x] && this.chunks[x][y - 1]) closestChunks.push(this.chunks[x][y - 1]);
	if (this.chunks[x + 1] && this.chunks[x + 1][y - 1]) closestChunks.push(this.chunks[x + 1][y - 1]);

	if (this.chunks[x - 1] && this.chunks[x - 1][y]) closestChunks.push(this.chunks[x - 1][y]);
	if (this.chunks[x] && this.chunks[x][y]) closestChunks.push(this.chunks[x][y]);
	if (this.chunks[x + 1] && this.chunks[x + 1][y]) closestChunks.push(this.chunks[x + 1][y]);

	if (this.chunks[x - 1] && this.chunks[x - 1][y + 1]) closestChunks.push(this.chunks[x - 1][y + 1]);
	if (this.chunks[x] && this.chunks[x][y + 1]) closestChunks.push(this.chunks[x][y + 1]);
	if (this.chunks[x + 1] && this.chunks[x + 1][y + 1]) closestChunks.push(this.chunks[x + 1][y + 1]);

	return closestChunks;

};



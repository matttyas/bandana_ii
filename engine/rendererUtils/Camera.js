function Camera (x, y) {
	this.x = x || 0;
	this.y = y || 0;
	this.followDistance = {x: 0, y: 0}
	this.lastPosition = null;
	this.movement = {x: 0, y: 0};
	this.target = null;
}

Camera.prototype.follow = function(target) {
	this.target = target;
};

Camera.prototype.setFollowDistance = function(x, y) {
	this.followDistance.x = x;
	this.followDistance.y = y;
};

Camera.prototype.update = function() {
	if (this.target) {
		this.lastPosition = {x: this.x, y: this.y};

		this.x = this.target.x - this.followDistance.x;
		this.y = this.target.y - this.followDistance.y;

		this.movement.x = this.lastPosition.x - this.x;
		this.movement.y = this.lastPosition.y - this.y;

	} else {
		//Unused. Suggest better positioning.
		this.x = 0;
		this.y = 0;
	}
};

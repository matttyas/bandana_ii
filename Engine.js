function Engine () {
  this.canvas=document.createElement("canvas");
	this.canvas.id = 'game';
	this.ctx=this.canvas.getContext("2d");
	var aspectRatio = window.innerWidth / window.innerHeight;

	this.canvas.width =  window.innerWidth;
	this.canvas.height = window.innerHeight;

	this.screenWidth = this.canvas.width;
	this.screenHeight = this.canvas.height;

	this.previousKeysDown = {};
	this.keysDown = {};

	this.stats = new Stats();
	this.stats.setMode(0); // 0: fps, 1: ms

	// Align top-left

	this.debug = false;

	this.stats.domElement.style.position = 'absolute';
	this.stats.domElement.style.left = '0px';
	this.stats.domElement.style.top = '60px';

		document.body.appendChild(this.canvas);
	if (this.debug) document.body.appendChild( this.stats.domElement );


	this.renderer = new Renderer(this.ctx, this.canvas.width, this.canvas.height);
	this.world = new World();
	//var murderBallScene = new MurderBallScene(this);
	var bandanaScene = new BandanaScene(this);
	this.currentScene = bandanaScene;


	this.renderer.setCurrentScene(this.currentScene);


}

Engine.prototype.update = function() {
	this.currentScene.handleKeyboard(this.keysDown, this.previousKeysDown);
	this.currentScene.update();

	this.renderer.render();
	engine.previousKeysDown = this.cloneKeysDown();
};

	//Clones the keys that are down at this particular moment
Engine.prototype.cloneKeysDown = function() {
  var tempKeysDown = {};
  for(var key in engine.keysDown) {
    tempKeysDown[key] = engine.keysDown[key];
  }
  return tempKeysDown;
}

var engine = new Engine();

//EVENT LISTENERS

addEventListener("keydown", function (e) {
	engine.keysDown[e.keyCode] = true;
}, false);


addEventListener("keyup", function (e) {
	delete engine.keysDown[e.keyCode];
}, false);


addEventListener("click", function(e) {
	var canvasRectangle = engine.canvas.getBoundingClientRect();

	var relX = e.clientX - canvasRectangle.left;
	var relY = e.clientY - canvasRectangle.top;

	if (engine.renderer.camera) {
		relX = relX + engine.renderer.camera.x;
		relY = relY + engine.renderer.camera.y;
	}

	engine.currentScene.handleClick(relX, relY);
}, false);



//THE GAME LOOP
var animFrame = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame    ||
        window.oRequestAnimationFrame      ||
        window.msRequestAnimationFrame     ||
        null ;

if(animFrame !== null) {
	var recursiveAnim = function() {
			engine.stats.begin();
      engine.update();
      engine.stats.end();
      animFrame( recursiveAnim );
	};

	animFrame( recursiveAnim );
} else {
    var ONE_FRAME_TIME = 1000.0 / 60.0 ;
    setInterval( main, ONE_FRAME_TIME );
}
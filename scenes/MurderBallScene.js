//Base scene, a template from which you can build your scenes.
function MurderBallScene(engine) {
  this.name = 'Murder Ball Scene';
  this.playerCount = 2;
  this.players = [];
  this.circleForces = [];
  this.engine = engine;
  this.ball = new Ball(396.5, 296.5, this.engine.screenWidth, this.engine.screenHeight);
  this.currentCheckpoint = {};
  this.initialise();
}

MurderBallScene.prototype = new Scene();

//Called once a frame by the engine.
MurderBallScene.prototype.update = function() {
	for (var i = 0; i < this.actors.length; i++) {
		this.actors[i].update();

    if (this.actors[i].spawnCircle) {
      var newCircleForce = new CircleForce(this.actors[i].x + (this.actors[i].width / 2), this.actors[i].y + (this.actors[i].height / 2), this.actors[i].color, this.actors[i].spawnForce, this.actors[i]);
      this.circleForces.push(newCircleForce);
      this.actors.push(newCircleForce);
      this.actors[i].setSpawnCircle(false);
    }
	}

  for ( var i = this.actors.length - 1; i >= 0; i--) {
    if (this.actors[i].deleteMe) {
      this.actors.splice(i, 1);
    }
  }

  for ( var i = this.circleForces.length - 1; i >= 0; i--) {
    if (this.circleForces[i].deleteMe) {
      this.circleForces.splice(i, 1);
    } else {

      //Move this into ball. Move circle intersect to math.
      if (this.ball.circlesIntersect({x: this.ball.x, y: this.ball.y, r: this.ball.width/2}, {x: this.circleForces[i].x, y: this.circleForces[i].y, r: this.circleForces[i].width/2})) {
        this.ball.movement.x = this.ball.x - this.circleForces[i].x;
        this.ball.movement.y = this.ball.y - this.circleForces[i].y;
      } else {
        console.log ('a');
      }
    }
  }

};

MurderBallScene.prototype.handleKeyboard = function(keysDown, previousKeysDown) {
	if ( 87 in keysDown) { //W

  }

  if ( 83 in keysDown) { //S

  }

  if ( 65 in keysDown) { //A

  }

  if (68 in keysDown) { //D

  }

  for (var i = 0; i < this.players.length; i++) {
    this.players[i].handleKeyboard(keysDown, previousKeysDown);
  }
};

MurderBallScene.prototype.initialise = function() {
  this.actors = [];

  this.setupPlayers();

  this.goal1 = new Goal(0, 250);
  this.actors.push(this.goal1);

  this.goal2 = new Goal(770, 250);
  this.actors.push(this.goal2);

  this.ball = new Ball(396.5, 296.5, this.engine.screenWidth, this.engine.screenHeight);
  this.actors.push(this.ball);
}

MurderBallScene.prototype.setupPlayers = function () {
  this.players = [];
  for (var i = 0; i < this.playerCount; i++) {
    switch (i) {
      case 0:
        var newPlayer = new Player(200, 290, '#FFA500', [87, 83, 65, 68, 32]);
        break;
      case 1:
        var newPlayer = new Player(580, 290, '#800080', [38, 40, 37, 39, 93]);
        break;
      case 2:

        break;
      case 3:

        break;
    }
    this.players.push(newPlayer);
    this.actors.push(newPlayer);
  }
}
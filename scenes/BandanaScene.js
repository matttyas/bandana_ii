//Base scene, a template from which you can build your scenes.
function BandanaScene(engine) {
  this.name = 'Bandana Scene';
  this.playerCount = 2;
  this.players = [];
  this.engine = engine;
  this.hero = new MrBandana();
  this.interface = new Interface();

  this.outScrollingBackground = new LevelScrollingBackground();

  this.backSB = new MountainSB();
  this.backSB.setOffsetHalfWidth();
  this.backSB.setTransparency(0.8);
  this.backSB.setScrollSpeed(12);
  this.backSB.increaseHeight(45);
  this.outScrollingBackground.addElement(this.backSB);

  this.scrollingBackground = new MountainSB();
  this.scrollingBackground.increaseHeight(-15);
  this.outScrollingBackground.addElement(this.scrollingBackground);

  this.stalagSB = new StalagtitesSB();
  this.stalagSB.increaseHeight(45);

  this.frontStalagSB = new StalagtitesSB();

  this.frontStalagSB.height = 150;
  this.frontStalagSB.setTransparency(0.8);
  this.frontStalagSB.setScrollSpeed(8);
  this.frontStalagSB.setInterval(200);
  this.frontStalagSB.width = 100;

  this.inScrollingBackground = new LevelScrollingBackground();
  this.inScrollingBackground.addElement(this.frontStalagSB);
  this.inScrollingBackground.addElement(this.stalagSB);


  //this.inScrollingBackground.addElement(this.backSB);

  I:this.levelScrollingBackground = this.outScrollingBackground;
//  this.levelScrollingBackground = this.inScrollingBackground;

  this.useCamera = true;
  this.backPanels = [];
  this.platforms = [];
  this.shinies = [];
  this.deathTraps = [];
  this.checkpoints = [];
  this.allCheckpoints = []; //All the checkpoints in the game. We'll use this to tie shinies to specific checkpoints.
  this.devices = []; //Switches, Gates, Keys, Doors?
  this.enemies = [];
  this.activeCheckpoint = {};

  //START
  this.hero.x = -2380;
  this.hero.y = 280;
  //this.hero.y = 300;

  //The first pit
  //this.hero.x = 866;
  //this.hero.y = 914;

  //Spike pit
  //this.hero.x = 3265;
  //this.hero.y = 1550;

  //this.hero.x = 3040;
  //this.hero.y = -436

  //this.hero.x = 3423;
  //this.hero.y = 779;
  //Top of canyon trees

  //this.hero.x = 4308;
  //this.hero.y = 224;

  //Temple
  //this.hero.x = 7012;
  //this.hero.y = 261;

  //The Caves Below
  //this.hero.x = 7878;
  //this.hero.y = 1597;

  //The Descent
  //this.hero.x = 6833;
  //this.hero.y = 3137;

  //The Great Mine

  //this.hero.x = 5792;
  //this.hero.y = 1797;

  //this.engine.renderer.hideScrollingBack = true;
  //this.engine.renderer.useGradientForBack = false;
  //this.engine.renderer.backColor = '#000000';
  //this.useBackParticleSystem = true;

  this.particleSystem = new ParticleSystem(this.hero.x - (this.engine.screenWidth / 2), this.hero.y - (this.engine.screenHeight / 2), null, null, 100, engine);
  this.particleSystem.setFollowCamera(true);

  this.chunker = new BandanaChunker();

  this.initialise();
}

BandanaScene.prototype = new Scene();

//Called once a frame by the engine.
BandanaScene.prototype.update = function() {

  for (var i = this.platforms.length - 1; i >= 0; i--) {
    this.platforms[i].update();
  }

  for (var i = this.enemies.length - 1; i >= 0; i--) {
    this.enemies[i].update();
  }

  if (this.useBackParticleSystem) this.particleSystem.update();

  this.hero.update();
  this.interface.update();

  this.levelScrollingBackground.scroll(this.hero);
  //this.particleSystem.scroll(this.hero);
	//for (var i = 0; i < this.actors.length; i++) {
		//this.actors[i].update();
//	}

  for ( var i = this.actors.length - 1; i >= 0; i--) {
    if (this.actors[i].deleteMe) {
      this.actors.splice(i, 1);
    }
  }

  for ( var i = this.shinies.length - 1; i >= 0; i--) {
    var collision = Physics.checkCollisionRects(this.hero, this.shinies[i]);
    if (collision.collision) {
      this.shinies[i].handleCollision(this.hero);
    }

    this.interface.shinies = this.hero.shinies;
  }

  for ( var i = this.deathTraps.length - 1; i >= 0; i--) {
    var collision = Physics.checkCollisionRects(this.hero, this.deathTraps[i]);
    if (collision.collision) {
      this.hero.dead = true;
    }
  }

  for ( var i = this.enemies.length - 1; i >= 0; i--) {
    var collision = Physics.checkCollisionRects(this.hero, this.enemies[i]);
    if (collision.collision) {
      this.enemies[i].handleCollision(this.hero);
    }
  }

  for ( var i = this.checkpoints.length - 1; i >= 0; i--) {
    var collision = Physics.checkCollisionRects(this.hero, this.checkpoints[i]);
    if (collision.collision) {
      this.resetCheckpoints();
      this.checkpoints[i].handleCollision(this.hero);
      this.activeCheckpoint = this.checkpoints[i];
    }
  }

  var deviceCollision = false;

  for ( var i = this.devices.length - 1; i >= 0; i--) {
    var collision = Physics.checkCollisionRects(this.hero, this.devices[i]);
    if (collision.collision) {
      deviceCollision = true;
      this.devices[i].handleCollision(this.hero);
    }
  }

  if (!deviceCollision) {
    this.hero.ladder = null;
    this.hero.onLadder = false;
  }

  //this.hero.setObstacles(this.platforms);
  this.hero.setDevices(this.devices);

  if (this.hero.dead) {
    this.respawn();
  }

  this.useChunks();
};

BandanaScene.prototype.resetCheckpoints = function() {
  for (var i = 0; i < this.checkpoints.length; i++) {
    this.checkpoints[i].active = false;
  }
};

BandanaScene.prototype.respawn = function() {

  if (! this.activeCheckpoint) this.activeCheckpoint = this.allCheckpoints[0];
  this.hero.x = this.activeCheckpoint.x + 4;
  this.hero.y = this.activeCheckpoint.y;
  this.hero.inair = true;
  this.hero.movement.x = 0;
  this.hero.movement.y = 0;
  this.hero.movementVector.x = 0;
  this.hero.dead = false;
};

BandanaScene.prototype.handleKeyboard = function(keysDown, previousKeysDown) {
	if ( 87 in keysDown) { //W

  }

  if ( 83 in keysDown) { //S

  }

  if ( 65 in keysDown) { //A

  }

  if (68 in keysDown) { //D

  }

  this.hero.handleKeyboard(keysDown, previousKeysDown);
};

BandanaScene.prototype.handleClick = function(x, y) {
  var debugActor = null;
  for (var i = 0; i < this.platforms.length; i++) {
    var actor = this.platforms[i];
    if (x > actor.x && x < actor.x + actor.width) {
      if (y > actor.y && y < actor.y + actor.height) {
        debugActor = actor;
      }
    }
  }

  if (debugActor) {
    this.debugActor(debugActor);
    return;
  }

  for (var i = 0; i < this.backPanels.length; i++) {
    var actor = this.backPanels[i];
    if (x > actor.x && x < actor.x + actor.width) {
      if (y > actor.y && y < actor.y + actor.height) {
        debugActor = actor;
      }
    }
  }

  if (debugActor) {
    this.debugActor(debugActor);
    return;
  }
};

BandanaScene.prototype.debugActor = function(actor) {
  console.log('Actor Name: ' + actor.name);
  console.log('x: ' + actor.x);
  console.log('y: ' + actor.y);
  console.log('width: ' + actor.width);
  console.log('height: ' + actor.height);
  console.log('right: ' + (actor.x + actor.width));
  console.log('bottom:  ' + (actor.y + actor.height));
  if (actor.canMove) console.log('canMove: true');
};

BandanaScene.prototype.initialise = function() {
  this.actors = [];
  this.world = this.engine.world.buildWorld(this.engine, this);
  this.chunker.chunk(this.world);

  this.allCheckpoints = this.world.checkpoints;
  /*this.actors = this.world.objects;
  /this.platforms = this.world.platforms;
  this.checkpoints = this.allCheckpoints;
  this.shinies = this.world.shinies;
  this.deathTraps = this.world.deathTraps;*/

  this.activeCheckpoint = this.checkpoints[0];



  this.actors.push(this.hero);
  this.interface = new Interface();
  this.actors.push(this.interface);

  this.engine.renderer.camera.follow(this.hero);

  var followX = this.engine.screenWidth / 2 - this.hero.width / 2;
  var followY = this.engine.screenHeight / 2 - this.hero.height / 2;
  this.engine.renderer.camera.setFollowDistance(followX, followY);

  this.useChunks();

  //this.actors.push(this.goal1);
};

//Builds object containing shinies indexed by id.
BandanaScene.prototype.buildTempShinyCheckpointIndex = function(shinies) {
  var indexedShinies = {};

  for (var i = 0; i < shinies.length; i++) {
    if (indexedShinies[shinies[i].checkpointId]) {
      indexedShinies[shinies[i].checkpointId].push(shinies[i]);
    } else {
      indexedShinies[shinies[i].checkpointId] = [];
      indexedShinies[shinies[i].checkpointId].push(shinies[i]);
    }
  }

  return indexedShinies;
};

BandanaScene.prototype.useChunks = function() {
  var chunks = this.chunker.getClosestChunks(this.hero);
  if (!chunks) return;

  this.actors =  [];
  this.platforms = [];
  this.checkpoints = [];
  this.shinies = [];
  this.deathTraps = [];
  this.backPanels = [];
  this.objects = []; //stupid nonsense.
  this.devices = [];
  this.enemies = [];
  //console.log(chunks);

  for (var i = 0; i < chunks.length; i++) {
    for (prop in chunks[i]) {
   //   console.log(prop);
      this[prop] = this[prop].concat(chunks[i][prop]);
    }
  }

  this.actors = this['objects'];
//  delete(this['objects']);

  if (this.shinies && this.shinies.length > 0) {
    var tempShinyIndex = this.buildTempShinyCheckpointIndex(this.shinies);
    //console.log(tempShinyIndex);

    for (var i = 0; i < this.allCheckpoints.length; i++) {

      this.allCheckpoints[i].addShinies(tempShinyIndex[this.allCheckpoints[i].id]);
      this.actors.push(this.allCheckpoints[i]);
    }
  }

  this.actors.push(this.backSB);
  this.actors.push(this.scrollingBackground);
  this.actors.push(this.interface);
  this.actors.push(this.hero);

  this.colliders = [];

  for (var i = 0; i < this.devices.length; i++) {
    if (this.devices[i].name == 'Door') {
      this.colliders.push(this.devices[i]);
    }
  }

  this.colliders = this.colliders.concat(this.platforms);
  this.hero.setObstacles(this.colliders);
}